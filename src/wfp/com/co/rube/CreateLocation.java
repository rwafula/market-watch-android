/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import wfp.com.co.rube.db.DbManager;
import wfp.com.co.rube.entities.Country;
import wfp.com.co.rube.entities.Location;

/**
 *
 * @author rube
 */
public class CreateLocation extends Fragment implements Comparator<Country> {

    private DbManager dbManager;
    private final static String TAG = "wfp.CreateLocation";
    public final static String ITEM_NAME = "Create Location";
    private Context context;
    private ArrayAdapter<String> countyAdapter;
    Spinner countyEditField;

    /**
     * Called when the activity is first created.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.new_location_layout_form, container, false);
        context = getActivity().getApplication();

        final Spinner stateEditField = (Spinner) view.findViewById(R.id.location_layout_state);
        countyEditField = (Spinner) view.findViewById(R.id.location_layout_county);
        //final EditText payamEditField = (EditText)view.findViewById(R.id.location_layout_payam);
        final EditText marketEditField = (EditText) view.findViewById(R.id.location_layout_market);

        ArrayList<String> stateList = getStates();
        //measureList.add(0, "Select item measure");

        ArrayList<String> countyList = getCounties(null);
        //locationList.add(0, "Select Location");

        ArrayAdapter<String> statesAdapter = new ArrayAdapter<String>(context,
                R.layout.spinner_item, stateList);
        statesAdapter.setDropDownViewResource(R.layout.spinner_layout);
        stateEditField.setAdapter(statesAdapter);
        stateEditField.setOnItemSelectedListener(new StateSpinnerOnItemSelectedListener());

        countyAdapter = new ArrayAdapter<String>(context,
                R.layout.spinner_item, countyList);
        countyAdapter.setDropDownViewResource(R.layout.spinner_layout);
        countyEditField.setAdapter(countyAdapter);

        Button submitButton = (Button) view.findViewById(R.id.location_layout_btn_submit);

        Button cancelButton = (Button) view.findViewById(R.id.location_layout_btn_cancel);

        if (this.dbManager == null) {
            this.dbManager = new DbManager(getActivity().getApplicationContext());
        }

        submitButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Location location = new Location();

                location.setStateName(stateEditField.getSelectedItem().toString());
                location.setCounty(countyEditField.getSelectedItem().toString());
                location.setMarket(marketEditField.getEditableText().toString());

                long locationId = dbManager.insertLocation(location);
                Log.d(TAG, "Location  submit done id:+ " + locationId + " LOC: " + location);
                loadLocationList();

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                loadLocationList();
            }
        });

        return view;
    }

    private ArrayList<String> getStates() {
        ArrayList<String> states = new ArrayList<String>();

        states.add("Eastern Equatoria State (EES)");
        states.add("Central Equatoria State(CES)");
        states.add("Western Equatoria State(WES)");
        states.add("Jonglei");
        states.add("Lakes");
        states.add("Northern Bahr el Ghazal");
        states.add("Western Bahr el Ghazal");
        states.add("Upper Nile");
        states.add("Unity");
        states.add("Warrap");
        return states;
    }

    private ArrayList<String> getCounties(String state) {
        ArrayList<String> counties = new ArrayList<String>();

        if (state == null || state.equals("Eastern Equatoria State (EES)")) {
            counties.add("Torit");
            counties.add("Magwi");
            counties.add("Ikotos");
            counties.add("Budi");
            counties.add("Kapoeta South");
            counties.add("Kapoeta North");
            counties.add("Kapoeta East");
            counties.add("Lopa/Lafor");
        }

        if (state == null || state.equals("Central Equatoria State(CES)")) {
            counties.add("Juba");
            counties.add("Lainya");
            counties.add("Yei- river");
            counties.add("Morobo");
            counties.add("Terekeke");

        }

        if (state == null || state.equals("Western Equatoria State(WES)")) {
            counties.add("Tambura");
            counties.add("Mundri East");
            counties.add("Nzara");
            counties.add("Ezo");
            counties.add("Yambio");
            counties.add("Maridi");
            counties.add("Mundri west");
            counties.add("Ibbi");
            counties.add("Movlo");
        }

        if (state == null || state.equals("Jonglei")) {
            counties.add("Bor");
        }

        if (state == null || state.equals("Lakes")) {
            counties.add("Rumbek");
        }
        if (state == null || state.equals("Northern Bahr el Ghazal")) {
            counties.add("Aweil Centre");
        }
        if (state == null || state.equals("Western Bahr el Ghazal")) {
            counties.add("Wau");
        }
        if (state == null || state.equals("Upper Nile")) {
            counties.add("Malakal");
        }
        if (state == null || state.equals("Unity")) {
            counties.add("Bentiu");
        }
        if (state == null || state.equals("Warrap")) {
            counties.add("Georial West");
        }

        return counties;
    }

    public void loadLocationList() {
        Fragment fragment = new LocationList();
        Bundle args = new Bundle();
        args.putString(ItemList.ITEM_NAME, "Location List");
        fragment.setArguments(args);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                .commit();
        getActivity().getActionBar().setTitle(LocationList.ITEM_NAME);
    }

    /**
     * Support sorting the countries list
     *
     * @param lhs
     * @param rhs
     * @return
     */
    @Override
    public int compare(Country lhs, Country rhs) {
        return lhs.getName().compareTo(rhs.getName());
    }

    private class StateSpinnerOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            String state = parent.getItemAtPosition(pos).toString();
            ArrayList<String> countyList = getCounties(state);
            if (context == null) {
                context = getActivity().getApplicationContext();
            }
            countyAdapter = new ArrayAdapter<String>(context,
                    R.layout.spinner_item, countyList);
            countyAdapter.setDropDownViewResource(R.layout.spinner_layout);

            countyEditField.setAdapter(countyAdapter);

            countyAdapter.notifyDataSetChanged();
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
            // TODO Auto-generated method stub
        }

    }

}

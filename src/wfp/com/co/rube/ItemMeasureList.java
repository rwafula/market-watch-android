/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import wfp.com.co.rube.adapters.ItemMeasureListAdaptor;
import wfp.com.co.rube.db.DbManager;
import wfp.com.co.rube.entities.ItemMeasure;

/**
 *
 * @author rube
 */
public class ItemMeasureList extends Fragment {
  public static final String ITEM_NAME = "Item Measure List";
    private View viewRoot;
    private final static String TAG = "wfp.ItemMeasure";
    private DbManager dbManager;
    private ListView view;
    private ItemMeasureListAdaptor itemMeasureListAdaptor;
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
        final Context context;
        context = this.getActivity().getApplicationContext();
        
        final LinearLayout layout ;
        layout  = (LinearLayout)inflater.inflate(R.layout.item_measure_list_layout, container, false);
        
        view = (ListView)layout.findViewById(R.id.item_measure_layout_list_view);
              
        Button addItemButton = (Button)layout.findViewById(R.id.add_item_measure_button);
        addItemButton.setOnClickListener(new View.OnClickListener() { 
            @Override
            public void onClick(View arg0) {
                Fragment fragment = new CreateItemMeasure();
                Bundle args = new Bundle();   
                args.putString(CreateItemMeasure.ITEM_NAME,"Item Measures");
                fragment.setArguments(args);                
                FragmentManager fragmentManager = getFragmentManager();        
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                .commit();
                getActivity().getActionBar().setTitle(CreateItem.ITEM_NAME);
                MarketWatch.backButtonEnabled = false;
            }
        });
        
        final EditText searchBoxField = (EditText) layout.findViewById(R.id.measure_item_input);
        
        searchBoxField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchText = searchBoxField.getEditableText().toString();
                    searchItemMeasureList(searchText);
                    
                    return true;
                }
                return false;
            }
        });
        
        searchBoxField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                            int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                            int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                    searchItemMeasureList(s.toString());
            }
        });
        
        ArrayList<ItemMeasure> itemMeasures = getItemMeasureList();   
        itemMeasureListAdaptor = new ItemMeasureListAdaptor(context, itemMeasures);
        view.setAdapter(itemMeasureListAdaptor);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = view.getItemAtPosition(position);
                        ItemMeasure measures = (ItemMeasure) o;
                        Toast.makeText(context, "Selected :" + " " + measures, Toast.LENGTH_LONG).show();
                }

        });
        
        viewRoot = layout;
        return layout;
    }
    
    public ArrayList<ItemMeasure> getItemMeasureList(){
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        return dbManager.queryItemMeasure(null, null,null, null, null);
    }
    
    public void searchItemMeasureList(String text){
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        ArrayList<ItemMeasure> items = dbManager.searchItemMeasures(text);
        
        itemMeasureListAdaptor =  
                new ItemMeasureListAdaptor(getActivity().getApplicationContext(), items);
        Log.d(TAG, "Notifying list cHANGE ..."+items);
        view.setAdapter(itemMeasureListAdaptor); 
        view.invalidateViews();
    }
    
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewRoot != null) {
            ViewGroup parentViewGroup = (ViewGroup) viewRoot.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    
   
    
}

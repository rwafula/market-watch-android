/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import org.json.JSONObject;
import wfp.com.co.rube.adapters.CountryListAdapter;
import wfp.com.co.rube.entities.Country;

/**
 *
 * @author rube
 */
public class CountryList extends Fragment implements
		      Comparator<Country>  {
    public static final String ITEM_NAME = "County List";
    private View viewRoot;
    private final static String TAG = "wfp.CountryList";
    private ListView view;
    
    /**
    * Hold all countries, sorted by country name
    */
    private static  List<Country> allCountries;
    
    /**
     * Hold countries that matched user query
     */
    private static List<Country> selectedCountriesList;
    
    private CountryListAdapter adapter;
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
        final Context context;
        context = this.getActivity().getApplicationContext();
        
        final LinearLayout layout ;
        layout  = (LinearLayout)inflater.inflate(R.layout.country_list_layout, container, false);

        EditText searchEditText = (EditText) layout
				.findViewById(R.id.search_country_input);
        
        view = (ListView)layout.findViewById(R.id.country_list_layout_view);
              
        List<Country> countries = getCountryData(context, this); 
        
        adapter = new CountryListAdapter(context, countries);
        view.setAdapter(adapter);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = view.getItemAtPosition(position);
                        Country country = (Country) o;
                        Toast.makeText(context, "Selected :" + " " + country, Toast.LENGTH_LONG).show();
                }

        });
        
        // Search for which countries matched user query
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                            int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                            int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                    countrySearch(s.toString());
            }
        });
        
        viewRoot = layout;
        return layout;
    }
    
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewRoot != null) {
            ViewGroup parentViewGroup = (ViewGroup) viewRoot.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    public static List<Country> getCountryData(Context context, Comparator<Country> comparable) {
        
        allCountries = new ArrayList<Country>();

        // Read from local file
        try{
            String allCountriesString = readFileAsString(context);
            //Log.d("countrypicker", "country: " + allCountriesString);
            JSONObject jsonObject = new JSONObject(allCountriesString);
            Iterator<?> keys = jsonObject.keys();

            // Add the data to all countries list
            while (keys.hasNext()) {
                String key = (String) keys.next();
                Country country = new Country();
                country.setCode(key);
                country.setName(jsonObject.getString(key));
                allCountries.add(country);
            }
        }catch(Exception ex){
            Log.e(TAG, "Exception "+ex.getMessage());
            ex.printStackTrace();
        }

        Collections.sort(allCountries, comparable);
        
        selectedCountriesList = new ArrayList<Country>();
        
        selectedCountriesList.addAll(allCountries);

        return  selectedCountriesList;
   

    }  
    
    /**
     * R.string.countries is a json string which is Base64 encoded to avoid
     * special characters in XML. It's Base64 decoded here to get original json.
     * 
     * @param context
     * @return
     * @throws java.io.IOException
     */
    private static String readFileAsString(Context context)
                    throws java.io.IOException {
            String base64 = context.getResources().getString(R.string.countries);
            byte[] data = Base64.decode(base64, Base64.DEFAULT);
            return new String(data, "UTF-8");
    }
    
    /**
    * Search allCountriesList contains text and put result into
    * selectedCountriesList
    * 
    * @param text
    */
   @SuppressLint("DefaultLocale")
   private void countrySearch(String text) {
       selectedCountriesList = new ArrayList<Country>();
           
        for (Country country : allCountries) {
                if (country.getName().toLowerCase(Locale.ENGLISH)
                                .contains(text.toLowerCase())) {
                        selectedCountriesList.add(country);
                }
        }
        adapter = new CountryListAdapter(getActivity().getApplicationContext(), selectedCountriesList);
        view.setAdapter(adapter); 
        view.invalidateViews();
   }
    
    /**
    * Support sorting the countries list
     * @param lhs
     * @param rhs
     * @return 
    */
   @Override
   public int compare(Country lhs, Country rhs) {
           return lhs.getName().compareTo(rhs.getName());
   }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import wfp.com.co.rube.adapters.ItemLayoutListAdapter;
import wfp.com.co.rube.entities.Item;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import wfp.com.co.rube.db.DbManager;

/**
 *
 * @author rube
 */
public class ItemList extends Fragment {
    public static final String ITEM_NAME = "Item List";
    private View viewRoot;
    private DbManager dbManager;
    
    private ItemLayoutListAdapter itemLayoutListAdapter;
    final private String TAG = "wfp.ItemList";
    private ListView view;
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
        final Context context;
        context = this.getActivity().getApplicationContext();
        
        final LinearLayout layout ;
        layout  = (LinearLayout)inflater.inflate(R.layout.item_list_layout, container, false);

        
        view = (ListView)layout.findViewById(R.id.item_layout_list_view);

        final EditText searchBoxField = (EditText) layout.findViewById(R.id.search_item_input);
        
        searchBoxField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchText = searchBoxField.getEditableText().toString();
                    searchItems(searchText);
                    
                    return true;
                }
                return false;
            }
        });
        
        searchBoxField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                            int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                            int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                    searchItems(s.toString());
            }
        });
        
        Button addItemButton = (Button)layout.findViewById(R.id.add_item_button);
        addItemButton.setOnClickListener(new View.OnClickListener() { 
            @Override
            public void onClick(View arg0) {
                Fragment fragment = new CreateItem();
                Bundle args = new Bundle();   
                args.putString(CreateItem.ITEM_NAME,"Items");
                fragment.setArguments(args);                
                FragmentManager fragmentManager = getFragmentManager();        
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                .commit();
                getActivity().getActionBar().setTitle(CreateItem.ITEM_NAME);
                MarketWatch.backButtonEnabled = false;
            }
        });
                
        List<Item> itemDetails = getListData();
        itemLayoutListAdapter = new ItemLayoutListAdapter(context, itemDetails);
        view.setAdapter(itemLayoutListAdapter);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = view.getItemAtPosition(position);
                        Item itemData = (Item) o;
                        Toast.makeText(context, "Selected :" + " " + itemData, Toast.LENGTH_LONG).show();
                }

        });
        
        viewRoot = layout;
        return layout;
    }
    
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewRoot != null) {
            ViewGroup parentViewGroup = (ViewGroup) viewRoot.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    private List<Item> getListData() {
        
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        return dbManager.queryItem(null, null,null, null, null);

    }  
    
    private void searchItems(String text){
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        ArrayList<Item> items = this.dbManager.searchItems(text);
        
        itemLayoutListAdapter =  
                new ItemLayoutListAdapter(getActivity().getApplicationContext(), items);
        Log.d(TAG, "Notifying list change ..."+items);
        view.setAdapter(itemLayoutListAdapter); 
        view.invalidateViews();
        
        
    }
    
    
}

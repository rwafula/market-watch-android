/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import wfp.com.co.rube.adapters.PriceResponseListAdapter;
import wfp.com.co.rube.db.DbManager;
import wfp.com.co.rube.entities.PriceResponse;

/**
 *
 * @author rube
 */
public class PriceResponseList extends Fragment{
    
    public static final String ITEM_NAME = "Price Responses";
    private View viewRoot;
    private DbManager dbManager;
    private String TAG = "wfp.PriceResponseList";
    
    private PriceResponseListAdapter priceResponseListAdapter;
    private ListView view;
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
        final Context context;
        context = this.getActivity().getApplicationContext();
        
        final LinearLayout layout ;
        layout  = (LinearLayout)inflater.inflate(R.layout.price_response_list_layout, container, false);

        
        view = (ListView)layout.findViewById(R.id.price_response_list_view);
        
        final EditText searchBoxField = (EditText) layout.findViewById(R.id.price_response_search_input);
        
        searchBoxField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchText = searchBoxField.getEditableText().toString();
                    searchPriceResponseList(searchText);
                    
                    return true;
                }
                return false;
            }
        });
        
        searchBoxField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                            int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                            int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                    searchPriceResponseList(s.toString());
            }
        });
                
        List<PriceResponse> responses = getListData();
        priceResponseListAdapter = new PriceResponseListAdapter(context, responses);
        
        view.setAdapter(priceResponseListAdapter);
        
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = view.getItemAtPosition(position);
                        PriceResponse response = (PriceResponse) o;
                        Toast.makeText(context, "Selected :" + " " + response, Toast.LENGTH_LONG).show();
                }

        });
        
        viewRoot = layout;
        return layout;
    }
    
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewRoot != null) {
            ViewGroup parentViewGroup = (ViewGroup) viewRoot.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    private List<PriceResponse> getListData() {
        
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager(getActivity().getApplicationContext() );
        }
        
        return dbManager.queryPriceResponse(null, null,null, null, null);

    }  
    
    private void searchPriceResponseList(String text){
         if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        List<PriceResponse> priceResponses = this.dbManager.searchPriceResponses(text);
        
        priceResponseListAdapter =  
                new PriceResponseListAdapter(getActivity().getApplicationContext(), priceResponses);
        Log.d(TAG, "Notifying list cHANGE ..."+priceResponses);
        
        view.setAdapter(priceResponseListAdapter); 
        view.invalidateViews();
    
    }
    
    
}

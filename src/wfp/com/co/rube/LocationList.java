/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import wfp.com.co.rube.adapters.LocationListAdapter;
import wfp.com.co.rube.db.DbManager;
import wfp.com.co.rube.entities.Location;

/**
 *
 * @author rube
 */
public class LocationList extends Fragment {
    
 public static final String ITEM_NAME = "Location List";
    private View viewRoot;
    private DbManager dbManager;
    private final String TAG ="wfp.LocationList";
    private LocationListAdapter locationListAdapter;
    private ListView view;
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
        final Context context;
        context = this.getActivity().getApplicationContext();
        
        final LinearLayout layout ;
        layout  = (LinearLayout)inflater.inflate(R.layout.location_list_layout, container, false);

        
        view = (ListView)layout.findViewById(R.id.location_layout_list_view);
        
        final EditText searchBoxField = (EditText) layout.findViewById(R.id.search_location_input);
        
        searchBoxField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchText = searchBoxField.getEditableText().toString();
                    searchLocationList(searchText);
                    
                    return true;
                }
                return false;
            }
        });
        
        searchBoxField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                            int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                            int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                    searchLocationList(s.toString());
            }
        });
        
        Button addItemButton = (Button)layout.findViewById(R.id.add_location_button);
        addItemButton.setOnClickListener(new View.OnClickListener() { 
            @Override
            public void onClick(View arg0) {
                Fragment fragment = new CreateLocation();
                Bundle args = new Bundle();   
                args.putString(CreateLocation.ITEM_NAME,"New Location");
                fragment.setArguments(args);                
                FragmentManager fragmentManager = getFragmentManager();        
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                .commit();
                getActivity().getActionBar().setTitle(CreateLocation.ITEM_NAME);
                MarketWatch.backButtonEnabled = false;
            }
        });
                
        List<Location> locations = getListData();
        locationListAdapter = new LocationListAdapter(context, locations);
        view.setAdapter(locationListAdapter);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = view.getItemAtPosition(position);
                        Location location = (Location) o;
                        Toast.makeText(context, "Selected :" + " " + location, Toast.LENGTH_LONG).show();
                }

        });
        
        
        viewRoot = layout;
        return layout;
    }
    
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewRoot != null) {
            ViewGroup parentViewGroup = (ViewGroup) viewRoot.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    private List<Location> getListData() {
        
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        return dbManager.queryLocation(null, null,null, null, null);

    }  
    
    private void searchLocationList(String text){
         if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        List<Location> locations = this.dbManager.searchLocations(text);
        
        locationListAdapter =  
                new LocationListAdapter(getActivity().getApplicationContext(), locations);
        Log.d(TAG, "Notifying list cHANGE ..."+locations);
        
        view.setAdapter(locationListAdapter); 
        view.invalidateViews();
    
    }
    
    
}

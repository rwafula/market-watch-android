/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

/**
 *
 * @author rube
 */
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import wfp.com.co.rube.db.DbManager;
import wfp.com.co.rube.entities.Item;
import wfp.com.co.rube.entities.ItemMeasure;
import wfp.com.co.rube.entities.ItemPrice;
import wfp.com.co.rube.entities.Location;

/**
 *
 * @author rube
 */
public class NewPrice extends Fragment{
    
    private DbManager dbManager;
    private final static String TAG = "wfp.NewPrice";
    public final static String ITEM_NAME = "New Price";
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
      
        View view = inflater.inflate(R.layout.new_market_price_form, container, false);
        Context context = getActivity().getApplicationContext();
        
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        final Spinner itemNameSpinnerField = (Spinner)view.findViewById(R.id.market_price_add_item_name);
        final Spinner measureSpinnerField = (Spinner)view.findViewById(R.id.market_price_add_measure);
        final Spinner locationSpinnerField = (Spinner)view.findViewById(R.id.market_price_add_location);
        final EditText priceEditField = (EditText)view.findViewById(R.id.market_price_add_price);
        
        ArrayList<String> measureList = getMeasures();
        //measureList.add(0, "Select item measure");
        
        ArrayList<String> locationList = getLocations();
        //locationList.add(0, "Select Location");
        
        ArrayList<String> itemList = getItems();
        ///itemList.add(0, "Select Item");
        
        ArrayAdapter<String> measureAdapter = new ArrayAdapter<String>(context,
                R.layout.spinner_item, measureList);
        measureAdapter.setDropDownViewResource(R.layout.spinner_layout);
        
        
        
        ArrayAdapter<String> locationAdapter = new ArrayAdapter<String>(context,
                R.layout.spinner_item, locationList);
        locationAdapter.setDropDownViewResource(R.layout.spinner_layout);
        
        ArrayAdapter<String> itemAdapter = new ArrayAdapter<String>(context,
                R.layout.spinner_item, itemList);
        itemAdapter.setDropDownViewResource(R.layout.spinner_layout);
        
        
        itemNameSpinnerField.setAdapter(itemAdapter);           
        locationSpinnerField.setAdapter(locationAdapter);       
        measureSpinnerField.setAdapter(measureAdapter);       
        
        Button submitButton = (Button)view.findViewById(R.id.market_price_btn_submit);        
        Button cancelButton = (Button)view.findViewById(R.id.market_price_btn_cancel);
        
        
        
        submitButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) 
            {
                String itemName = itemNameSpinnerField.getSelectedItem().toString();
                String measure = measureSpinnerField.getSelectedItem().toString();
                String location = locationSpinnerField.getSelectedItem().toString();
                String price = priceEditField.getEditableText().toString();
                
                ItemPrice itemPrice = new ItemPrice();
                
                itemPrice.setDate(
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                itemPrice.setItemName(itemName);
                itemPrice.setLocation(location);
                itemPrice.setPrice(price); 
                itemPrice.setMeasure(measure); 
                
                
                long id = dbManager.insertNewPrice(itemPrice);
                Log.d( TAG, "New prie submit done: " + id);
                loadPriceLists();
                
            }
        });
        
        cancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) 
            {
                loadPriceLists();                
            }
        });
 
        return view;
    }
    
    public void loadPriceLists(){
        Fragment fragment = new MarketPrice();
        Bundle args = new Bundle();   
        args.putString(MarketPrice.ITEM_NAME,"Price List");
        fragment.setArguments(args);     

        FragmentManager fragmentManager = getFragmentManager();        
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
        .commit();
        getActivity().getActionBar().setTitle(MarketPrice.ITEM_NAME);
    }
    
    public ArrayList<String> getMeasures(){
        
        ArrayList<ItemMeasure> measures 
                = this.dbManager.queryMeasure(null, null, null, null, null);
        
        ArrayList<String> measureList = new ArrayList<String>();
        for(ItemMeasure measure: measures){
            measureList.add(measure.getItemName());
        }
        
        return measureList;
    
    }
    
    public ArrayList<String> getLocations(){
        
        List<Location> locations
                = this.dbManager.queryLocation(null, null, null, null, null);
        
        ArrayList<String> locationList = new ArrayList<String>();
        for(Location location: locations){                         
            locationList.add(location.getMarket());
        }
        
        return locationList;    
    }
    
    public ArrayList<String> getItems(){
        
        List<Item> items
                = this.dbManager.queryItem(null, null, null, null, null);
        
        ArrayList<String> itemList = new ArrayList<String>();
        for(Item item: items){                         
            itemList.add(item.getItemName());
        }
        
        Log.d(TAG, itemList.toString());
        return itemList;    
    }
    

}

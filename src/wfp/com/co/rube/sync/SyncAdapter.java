/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import wfp.com.co.rube.db.DbManager;
import wfp.com.co.rube.db.MarketWatch;
import wfp.com.co.rube.entities.Item;
import wfp.com.co.rube.entities.ItemMeasure;
import wfp.com.co.rube.entities.ItemPrice;
import wfp.com.co.rube.entities.Location;
import wfp.com.co.rube.entities.PriceRequest;
import wfp.com.co.rube.entities.PriceResponse;

/**
 *
 * @author rube
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String TAG = "wfp.SyncAdapter";

    //private static final String SERVER_ACCESS_USERNAME = "admin";
    //private static final String SERVER_ACCESS_PASSWORD = "admin";
    private DbManager dbManager;

    //private static final String SERVER_ACCESS_URL
    //        = "http://" + SERVER_ACCESS_USERNAME + ":"
    //        + SERVER_ACCESS_PASSWORD + "@192.168.43.109:3000/";
    
    private static final String SERVER_ACCESS_URL
            = "http://104.237.142.18:82/";

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        if (this.dbManager == null) {
            this.dbManager = new DbManager(context);
        }
        Log.e(TAG, "Calling syncadapter here ");
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
            ContentProviderClient contentProviderClient, SyncResult syncResult) {
        // naive implementation, delete and replace everything
        SyncResult result = new SyncResult();
        try {
            //items sync
            syncItems(contentProviderClient);
            // item measure
            syncItemMeasure(contentProviderClient);
            
            //location sync
            syncLocation(contentProviderClient);
            
            //syncPriceRequests
            
            syncPriceRequests(contentProviderClient);
            
            //syncPriceResponse            
            syncPriceResponse(contentProviderClient);
            
            // syncItemPrice
            syncItemPrice(contentProviderClient);
           
        } catch (RemoteException re) {
            syncResult.hasHardError();
        } catch (IOException e) {
            syncResult.hasHardError();
        }
    }

  
    

    //MarketWatch.ItemPrice.TABLE, ITEM_PRICE
    //MarketWatch.ItemPrice.TABLE + "/#", ITEM_PRICE_ID
    
    //MarketWatch.Location.TABLE, LOCATION
    //MarketWatch.Location.TABLE + "/#", LOCATION_ID
    
    //MarketWatch.PriceRequest.TABLE, PRICE_REQUEST
    //MarketWatch.PriceRequest.TABLE + "/#", PRICE_REQUEST_ID
    
    //MarketWatch.PriceResponse.TABLE, PRICE_RESPONSE
    //MarketWatch.PriceResponse.TABLE + "/#", PRICE_RESPONSE_ID
    
    private void syncItemPrice(ContentProviderClient contentProviderClient) throws 
            RemoteException, IOException{
        List<ItemPrice> existingItems = this.dbManager.queryItemPrice(
                    MarketWatch.ItemPrice.SERVER_ID + "= ? or "+ MarketWatch.ItemPrice.SERVER_ID+" is null ",
                new String[]{"0"}, null, null, null);

        if (!existingItems.isEmpty()) {
            //Shoot them to the server
            for (ItemPrice itemPrice : existingItems) {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("item", 
                        dbManager.getServerId(MarketWatch.Item.TABLE, " name = ? ", 
                                new String[]{itemPrice.getItemName()})));
                
                params.add(new BasicNameValuePair("location", 
                         dbManager.getServerId(MarketWatch.Location.TABLE, " market = ? ", 
                                new String[]{itemPrice.getLocation()})));   
                
                params.add(new BasicNameValuePair("measure",
                         dbManager.getServerId(MarketWatch.ItemMeasure.TABLE, " name = ? ", 
                                new String[]{itemPrice.getMeasure()}))); 
                
                params.add(new BasicNameValuePair(MarketWatch.ItemPrice.PRICE, 
                        itemPrice.getPrice()));
               
                String path = "api/price/";
                try {
                    Log.d(TAG, "Sending params to sever "+params);
                    HashMap<String, Object> response = postToServer(params, path);
                    Object serverId = response.get("id");                   
                    this.dbManager.updateItemPriceAsPosted(String.valueOf(serverId), 
                            itemPrice.getItemName(), itemPrice.getLocation(), 
                            itemPrice.getMeasure());
                    
                } catch (Exception ex) {
                    Log.d(TAG, "Error item price posting to server: " + ex.getMessage());
                    ex.printStackTrace();
                }
            }

        } else {
            Log.d(TAG, "Skipping insert to server no item price on client");
        }
        
    }
    
    //For responses we simply pull from the server
    private void syncPriceResponse(ContentProviderClient contentProviderClient) throws 
            RemoteException, IOException{
        
        String lastSynchDate = this.dbManager.getLastSyncDate(MarketWatch.PriceResponse.TABLE);
        String path = "api/response/";
        HashMap<String, Object>[] responseMap = readFromServer(lastSynchDate, path);
        
        try {
            
            for (HashMap<String, Object> itemMap : responseMap) {

                Log.d(TAG, "Inserting item  ... " + itemMap);

                List<PriceResponse> existingResponses = this.dbManager.queryPriceResponse(
                        "a."+MarketWatch.PriceResponse.REQUEST_ID + "= ? ",
                        new String[]{
                            String.valueOf(itemMap.get("request")), 
                            } , null, null, null);

                // only recreate if we are empty
                if (existingResponses.isEmpty()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(
                            MarketWatch.PriceResponse.REQUEST_ID, String.valueOf(itemMap.get("request")));
                    
                    contentValues.put(
                            MarketWatch.PriceResponse.MESSAGE, String.valueOf(itemMap.get("message")));
                   
                    //This f**k sh**t returns a double on int fields ... watch out!!!
                    Object serverId = itemMap.get("id");
                    Log.d(TAG, "ServerId " + String.valueOf(serverId));


                    contentProviderClient.insert(
                            MarketWatch.PriceResponse.CONTENT_URI, contentValues);
                } else {
                    Log.d(TAG, "Found duplicate item : " + itemMap.get("market"));
                }

            }

            // update sync table
            this.dbManager.updateSyncDate(MarketWatch.PriceResponse.TABLE, lastSynchDate);
        } catch (Exception ex) {
            Log.e(TAG, "Something failed price response  " + ex.getMessage());
            ex.printStackTrace();
        }
    
    }
    
    //We simply push your requests to the server
    private void syncPriceRequests(ContentProviderClient contentProviderClient) throws 
            RemoteException, IOException{
        
        List<PriceRequest> priceRequests = this.dbManager.queryPriceRequest(
                    "p."+MarketWatch.PriceRequest.SERVER_ID + "= ? or p."+ MarketWatch.PriceRequest.SERVER_ID+" is null ",
                new String[]{"0"}, null, null, null);

        if (!priceRequests.isEmpty()) {
            //Shoot them to the server
            for (PriceRequest request : priceRequests) {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("item", request.getItemName()));
                params.add(new BasicNameValuePair("measure", request.getMeasure()));
                params.add(new BasicNameValuePair("location", request.getLocation()));
               
               
                String path = "api/request/";
                try {
                    HashMap<String, Object> response = postToServer(params, path);
                    Object serverId = response.get("id");                   
                    this.dbManager.updatepriceRequestAsPosted(String.valueOf(serverId), 
                            request.getLocation(), request.getItemName(), request.getMeasure());
                } catch (Exception ex) {
                    Log.d(TAG, "Error location posting to server: " + ex.getMessage());
                    ex.printStackTrace();
                }
            }

        } else {
            Log.d(TAG, "Skipping insert to server no item location on client");
        }
    

    }
    
    private void syncLocation(ContentProviderClient contentProviderClient) throws 
            RemoteException, IOException{
         insertLocationFromServer(contentProviderClient);
         insertLocationToServer(contentProviderClient);
    
    }
    
    private void insertLocationToServer(ContentProviderClient contentProviderClient) throws 
            RemoteException, IOException{
        
         List<Location> existingLocations = this.dbManager.queryLocation(
                    MarketWatch.Location.SERVER_ID + "= ? or "+ MarketWatch.Location.SERVER_ID+" is null ",
                new String[]{"0"}, null, null, null);

        if (!existingLocations.isEmpty()) {
            //Shoot them to the server
            for (Location location : existingLocations) {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair(MarketWatch.Location.STATE, location.getStateName()));
                params.add(new BasicNameValuePair(MarketWatch.Location.COUNTY, location.getCounty()));                
                params.add(new BasicNameValuePair(MarketWatch.Location.MARKET, location.getMarket()));
               
                String path = "api/location/";
                try {
                    HashMap<String, Object> response = postToServer(params, path);
                    Object serverId = response.get("id");                   
                    this.dbManager.updateLocationAsPosted(String.valueOf(serverId), 
                            location.getMarket());
                } catch (Exception ex) {
                    Log.d(TAG, "Error location posting to server: " + ex.getMessage());
                    ex.printStackTrace();
                }
            }

        } else {
            Log.d(TAG, "Skipping insert to server no item location on client");
        }
    
    
    }
     
     
    private void insertLocationFromServer(ContentProviderClient contentProviderClient) throws 
        RemoteException, IOException{
        String lastSynchDate = this.dbManager.getLastSyncDate(MarketWatch.Location.TABLE);
        String path = "api/location/";
        HashMap<String, Object>[] responseMap = readFromServer(lastSynchDate, path);
        
        try {
            
            for (HashMap<String, Object> itemMap : responseMap) {

                Log.d(TAG, "Inserting item  ... " + itemMap);

                List<Location> existingLocations = this.dbManager.queryLocation(
                        MarketWatch.Location.MARKET + "= ?",
                        new String[]{
                            String.valueOf(itemMap.get("market")) 
                            } , null, null, null);

                // only recreate if we are empty
                if (existingLocations.isEmpty()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(
                            MarketWatch.Location.STATE, String.valueOf(itemMap.get("state")));
                    
                    contentValues.put(
                            MarketWatch.Location.COUNTY, String.valueOf(itemMap.get("county")));
                    
                    
                    contentValues.put(
                            MarketWatch.Location.MARKET, String.valueOf(itemMap.get("market")));
                    //This f**k sh**t returns a double on int fields ... watch out!!!
                    Object serverId = itemMap.get("id");
                    Log.d(TAG, "ServerId " + String.valueOf(serverId));
                    contentValues.put(
                            MarketWatch.ItemMeasure.SERVER_ID, String.valueOf(serverId));

                    contentProviderClient.insert(
                            MarketWatch.Location.CONTENT_URI, contentValues);
                } else {
                    Log.d(TAG, "Found duplicate item : " + itemMap.get("market"));
                }

            }

            // update sync table
            this.dbManager.updateSyncDate(MarketWatch.ItemMeasure.TABLE, lastSynchDate);
        } catch (Exception ex) {
            Log.e(TAG, "Something failed inserting item " + ex.getMessage());
            ex.printStackTrace();
        }
    
    }
    
    
    private void syncItemMeasure(ContentProviderClient contentProviderClient) throws 
            RemoteException, IOException{
         insertItemMeasureFromServer(contentProviderClient);
         insertItemMeasureToServer(contentProviderClient);
    
    }
    

    private void insertItemMeasureToServer(ContentProviderClient contentProviderClient)
            throws RemoteException, IOException {

        List<ItemMeasure> existingMeasures = this.dbManager.queryItemMeasure(
                    MarketWatch.ItemMeasure.SERVER_ID + "= ? or "+ MarketWatch.ItemMeasure.SERVER_ID+" is null ",
                new String[]{"0"}, null, null, null);

        if (!existingMeasures.isEmpty()) {
            //Shoot them to the server
            for (ItemMeasure itemMeasure : existingMeasures) {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("name", itemMeasure.getItemName()));
                String path = "api/item_measure/";
                try {
                    HashMap<String, Object> response = postToServer(params, path);
                    Object serverId = response.get("id");                   
                    this.dbManager.updateItemMeasureAsPosted(String.valueOf(serverId), 
                            itemMeasure.getItemName());
                } catch (Exception ex) {
                    Log.d(TAG, "Error item measure posting to server: " + ex.getMessage());
                    ex.printStackTrace();
                }
            }

        } else {
            Log.d(TAG, "Skipping insert to server no item measure on client");
        }

    }
    
    /**
     * Fetch data from the remote server and populate the local database via the
     * content provider (item measure)
     */
    private void insertItemMeasureFromServer(ContentProviderClient contentProviderClient)
            throws RemoteException, IOException {

        String lastSynchDate = this.dbManager.getLastSyncDate(MarketWatch.ItemMeasure.TABLE);
        String path = "api/item_measure/";
        HashMap<String, Object>[] responseMap = readFromServer(lastSynchDate, path);
        
        try {
            
            for (HashMap<String, Object> itemMap : responseMap) {

                Log.d(TAG, "Inserting item  ... " + itemMap);

                List<ItemMeasure> existingItems = this.dbManager.queryItemMeasure(
                        MarketWatch.ItemMeasure.NAME + "= ?",
                        new String[]{String.valueOf(itemMap.get("name"))}, null, null, null);

                // only recreate if we are empty
                if (existingItems.isEmpty()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(
                            MarketWatch.ItemMeasure.NAME, String.valueOf(itemMap.get("name")));
                    //This f**k sh**t returns a double on int fields ... watch out!!!
                    Object serverId = itemMap.get("id");
                    Log.d(TAG, "ServerId " + String.valueOf(serverId));
                    contentValues.put(
                            MarketWatch.ItemMeasure.SERVER_ID, String.valueOf(serverId));

                    contentProviderClient.insert(
                            MarketWatch.ItemMeasure.CONTENT_URI, contentValues);
                } else {
                    Log.d(TAG, "Found duplicate item : " + itemMap.get("name"));
                }

            }

            // update sync table
            this.dbManager.updateSyncDate(MarketWatch.ItemMeasure.TABLE, lastSynchDate);
        } catch (Exception ex) {
            Log.e(TAG, "Something failed inserting item " + ex.getMessage());
            ex.printStackTrace();
        }
    }
    
    
    private void syncItems(ContentProviderClient contentProviderClient) throws 
            RemoteException, IOException{
         insertItemsFromServer(contentProviderClient);
         insertItemsToServer(contentProviderClient);
    
    }
    
    /**
     * Fetch data from the remote server and populate the local database via the
     * content provider (item)
     */
    private void insertItemsFromServer(ContentProviderClient contentProviderClient)
            throws RemoteException, IOException {

        String lastSynchDate = this.dbManager.getLastSyncDate(MarketWatch.Item.TABLE);
        String path = "api/item/";
        HashMap<String, Object>[] responseMap = readFromServer(lastSynchDate, path);
        
        try {

            for (HashMap<String, Object> itemMap : responseMap) {

                Log.d(TAG, "Inserting item  ... " + itemMap);

                List<Item> existingItems = this.dbManager.queryItem(MarketWatch.Item.NAME + "= ?",
                        new String[]{String.valueOf(itemMap.get("name"))}, null, null, null);

                // only recreate if we are empty
                if (existingItems.isEmpty()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(
                            MarketWatch.Item.NAME, String.valueOf(itemMap.get("name")));
                    //This f**k sh**t returns a double on int fields ... watch out!!!
                    Object serverId = itemMap.get("id");
                    Log.d(TAG, "ServerId " + serverId);
                    contentValues.put(
                            MarketWatch.Item.SERVER_ID, String.valueOf(serverId));

                    contentProviderClient.insert(
                            MarketWatch.Item.CONTENT_URI, contentValues);
                } else {
                    Log.d(TAG, "Found duplicate item : " + itemMap.get("name"));
                }

            }

            // update sync table
            this.dbManager.updateSyncDate(MarketWatch.Item.TABLE, lastSynchDate);
        } catch (Exception ex) {
            Log.e(TAG, "Something failed inserting item " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    private void insertItemsToServer(ContentProviderClient contentProviderClient)
            throws RemoteException, IOException {

        List<Item> existingItems = this.dbManager.queryItem(MarketWatch.Item.SERVER_ID + "= ?",
                new String[]{"0"}, null, null, null);

        if (!existingItems.isEmpty()) {
            //Shoot them to the server
            for (Item item : existingItems) {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("name", item.getItemName()));
                String path = "api/item/";
                try {
                    HashMap<String, Object> response = postToServer(params, path);
                    Object serverId = response.get("id");                  
                    this.dbManager.updateItemAsPosted(String.valueOf(serverId), item.getItemName());
                } catch (Exception ex) {
                    Log.d(TAG, "Error posting to server: " + ex.getMessage());
                    ex.printStackTrace();
                }
            }

        } else {
            Log.d(TAG, "Skipping insert to server no items on client");
        }

    }

    private HashMap<String, Object>[] readFromServer(String lastSyncDate, String path) {
        URL url;
        try {

            if (lastSyncDate != null) {
                String query = "sync_date=" + URLEncoder.encode(lastSyncDate, "utf-8");
                url = new URL(SERVER_ACCESS_URL + path+ "?" + query);
            } else {
                url = new URL(SERVER_ACCESS_URL + path);
            }

            Log.d(TAG, "Attempting connection open init.. readFromServer " + url.getPath());
            URLConnection urlConnection = url.openConnection();
            urlConnection.setRequestProperty("Accept-Charset", "utf-8");
            if (url.getUserInfo() != null) {
                Log.d(TAG, "User info null setting a new .. ");
                String basicAuth = "Basic " + new String(android.util.Base64.encode(
                        url.getUserInfo().getBytes(), android.util.Base64.DEFAULT));
                urlConnection.setRequestProperty("Authorization", basicAuth);
                
            }
            urlConnection.setDoOutput(false);
            

            BufferedReader bufReader = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
            
            Gson gson = new Gson();

            Long id = null;
            String responseMessage = new String();
            String line = null;
            // looping through rows
            while ((line = bufReader.readLine()) != null) {
                responseMessage += line;
            }
            //responseMessage = responseMessage.substring(1, responseMessage.length()-1);
            Log.d(TAG, "FOUND RESPONSE STRING " + responseMessage);

            JsonReader reader = new JsonReader(new StringReader(responseMessage));
            reader.setLenient(true);

            HashMap<String, Object>[] responseMap = gson.fromJson(reader, HashMap[].class);
            
            return responseMap;

        } catch (Exception ex) {
            Log.e(TAG, "Something failed inserting readFromServer " + ex.getMessage());
            ex.printStackTrace();
        }
        
        return null;
    }

    private HashMap<String, Object> postToServer(List<NameValuePair> params, String path) throws Exception {
        // Create a new HttpClient and Post Header
        URL url = new URL(SERVER_ACCESS_URL + path);
        Log.d(TAG, "Attempting connection open post conn.. " + url.getPath());
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestProperty("Accept-Charset", "utf-8");
        
        if (url.getUserInfo() != null) {
            Log.d(TAG, "User info null setting a new .. ");
            String basicAuth = "Basic " + new String(android.util.Base64.encode(
                    url.getUserInfo().getBytes(), android.util.Base64.DEFAULT));
            urlConnection.setRequestProperty("Authorization", basicAuth);
        }

        urlConnection.setRequestMethod("POST");
        urlConnection.setDoOutput(true);

        try {
            Log.d(TAG, "Trying to connect ...");
            OutputStreamWriter writer = new OutputStreamWriter(
                    urlConnection.getOutputStream());
            writer.write(getQuery(params));
            writer.flush();
            String line;
            String responseMessage = new String();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    urlConnection.getInputStream()));
            while ((line = reader.readLine()) != null) {
                Log.d(TAG, "Reading line by line: " + line);
                responseMessage += line;
            }
            
            
            Gson gson = new Gson();
            JsonReader jsonReader = new JsonReader(new StringReader(responseMessage));
            jsonReader.setLenient(true);

            HashMap<String, Object> responseMap = gson.fromJson(jsonReader, HashMap.class);
            
            writer.close();
            reader.close();
            urlConnection.disconnect();
            Log.d(TAG, "RESPONSE MAP " + responseMap);
            return responseMap ;
            
        } catch (Exception ex) {
            Log.e(TAG, "Something failed inserting postToServer " + ex.getMessage());
            ex.printStackTrace();
        }
        
        return null;

    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

}

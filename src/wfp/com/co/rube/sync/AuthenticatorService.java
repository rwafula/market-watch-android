/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;



/**
 *
 * @author rube
 */
public class AuthenticatorService extends Service {
    
  private Authenticator authenticator;
  
  @Override
  public void onCreate() {
    authenticator = new Authenticator(this);
  }
 
  /*
  * When the system binds to this Service to make the RPC call
  * return the authenticator’s IBinder.
  */
  @Override
  public IBinder onBind(Intent intent) {
    return authenticator.getIBinder();
  }
}
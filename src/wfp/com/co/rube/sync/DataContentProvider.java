/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.sync;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;
import java.util.List;
import wfp.com.co.rube.db.DbManager;
import wfp.com.co.rube.db.MarketWatch;
import wfp.com.co.rube.entities.ItemMeasure;
import wfp.com.co.rube.entities.Location;

/**
 *
 * @author rube
 */
public class DataContentProvider extends ContentProvider {

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private static final String AUTHORITY = MarketWatch.AUTHORITY;
    private static final String TAG = "wfp.DataContentProvider";
    
    private static final int COUNTRY = 1;
    private static final int COUNTRY_ID = 11;

    private static final int ITEM_MEASURE = 2;
    private static final int ITEM_MEASURE_ID = 22;

    private static final int ITEM = 3;
    private static final int ITEM_ID = 33;

    private static final int ITEM_PRICE = 4;
    private static final int ITEM_PRICE_ID = 44;

    private static final int LOCATION = 5;
    private static final int LOCATION_ID = 55;

    private static final int PRICE_REQUEST = 6;
    private static final int PRICE_REQUEST_ID = 66;

    private static final int PRICE_RESPONSE = 7;
    private static final int PRICE_RESPONSE_ID = 77;

    private DbManager dbManager;

    static {
        uriMatcher.addURI(AUTHORITY, MarketWatch.Country.TABLE, COUNTRY);
        uriMatcher.addURI(AUTHORITY, MarketWatch.Country.TABLE + "/#", COUNTRY_ID);

        uriMatcher.addURI(AUTHORITY, MarketWatch.ItemMeasure.TABLE, ITEM_MEASURE);
        uriMatcher.addURI(AUTHORITY, MarketWatch.ItemMeasure.TABLE + "/#", ITEM_MEASURE_ID);

        uriMatcher.addURI(AUTHORITY, MarketWatch.Item.TABLE, ITEM);
        uriMatcher.addURI(AUTHORITY, MarketWatch.Item.TABLE + "/#", ITEM_ID);

        uriMatcher.addURI(AUTHORITY, MarketWatch.ItemPrice.TABLE, ITEM_PRICE);
        uriMatcher.addURI(AUTHORITY, MarketWatch.ItemPrice.TABLE + "/#", ITEM_PRICE_ID);

        uriMatcher.addURI(AUTHORITY, MarketWatch.Location.TABLE, LOCATION);
        uriMatcher.addURI(AUTHORITY, MarketWatch.Location.TABLE + "/#", LOCATION_ID);

        uriMatcher.addURI(AUTHORITY, MarketWatch.PriceRequest.TABLE, PRICE_REQUEST);
        uriMatcher.addURI(AUTHORITY, MarketWatch.PriceRequest.TABLE + "/#", PRICE_REQUEST_ID);

        uriMatcher.addURI(AUTHORITY, MarketWatch.PriceResponse.TABLE, PRICE_RESPONSE);
        uriMatcher.addURI(AUTHORITY, MarketWatch.PriceResponse.TABLE + "/#", PRICE_RESPONSE_ID);
    }

    @Override
    public boolean onCreate() {
         Log.d(TAG, "Some one called contentPovider ..");
        if (dbManager == null) {
            dbManager = new DbManager(getContext());
        }
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] columns, String selection,
            String[] selectionArgs, String sortOrder) {
        
        List segments;
            segments = uri.getPathSegments();

        SQLiteDatabase sqldb = dbManager.getWritableDatabase();

        switch (uriMatcher.match(uri)) {
            case ITEM:
                return sqldb.query(MarketWatch.Item.TABLE, null,
                        selection, selectionArgs, null, null, sortOrder);
            case COUNTRY:
                return sqldb.query(MarketWatch.Country.TABLE, null,
                        selection, selectionArgs, null, null, sortOrder);

            case ITEM_MEASURE:
                return sqldb.query(MarketWatch.ItemMeasure.TABLE, null,
                        selection, selectionArgs, null, null, sortOrder);

            case ITEM_PRICE:
                return sqldb.query(MarketWatch.ItemPrice.TABLE, null,
                        selection, selectionArgs, null, null, sortOrder);

            case PRICE_REQUEST:
                return sqldb.query(MarketWatch.PriceRequest.TABLE, null,
                        selection, selectionArgs, null, null, sortOrder);

            case LOCATION:
                return sqldb.query(MarketWatch.Location.TABLE, null,
                        selection, selectionArgs, null, null, sortOrder);

            case PRICE_RESPONSE:
                return sqldb.query(MarketWatch.PriceResponse.TABLE, null,
                        selection, selectionArgs, null, null, sortOrder);

            default:
                throw new RuntimeException("No content provider URI match.");
        }
    }

    @Override
    public String getType(Uri uri) {

        switch (uriMatcher.match(uri)) {

            case ITEM:
                return MarketWatch.Item.CONTENT_TYPE;

            case ITEM_ID:
                return MarketWatch.Item.CONTENT_ITEM_TYPE;

            case COUNTRY:
                return MarketWatch.Country.CONTENT_TYPE;

            case COUNTRY_ID:
                return MarketWatch.Country.CONTENT_ITEM_TYPE;

            case ITEM_MEASURE:
                return MarketWatch.ItemMeasure.CONTENT_TYPE;
                
            case ITEM_MEASURE_ID:
                return MarketWatch.ItemMeasure.CONTENT_ITEM_TYPE;

            case ITEM_PRICE:
                return MarketWatch.ItemPrice.CONTENT_TYPE;

            case ITEM_PRICE_ID:
                return MarketWatch.ItemPrice.CONTENT_ITEM_TYPE;

            case PRICE_REQUEST:
                return MarketWatch.PriceRequest.CONTENT_TYPE;
                
            case PRICE_REQUEST_ID:
                return MarketWatch.PriceRequest.CONTENT_ITEM_TYPE;

            case LOCATION:
                return MarketWatch.Location.CONTENT_TYPE;

            case LOCATION_ID:
                return MarketWatch.Location.CONTENT_ITEM_TYPE;

            case PRICE_RESPONSE:
                return MarketWatch.PriceResponse.CONTENT_TYPE;
                
            case PRICE_RESPONSE_ID:
                return MarketWatch.PriceResponse.CONTENT_ITEM_TYPE;

            default:
                throw new RuntimeException("No content provider URI match.");
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        
    
        switch (uriMatcher.match(uri)) {

            case ITEM:
            case ITEM_ID:
                Log.d(TAG, "Hitting insert item on contentPovider ..");
                String itemName = contentValues.getAsString(MarketWatch.Item.NAME);
                String itemServerId= contentValues.getAsString(MarketWatch.Item.SERVER_ID);
                long itemId = this.dbManager.insertItem(itemName, itemServerId); 
                return Uri.withAppendedPath(MarketWatch.Item.CONTENT_URI, "" + itemId);                


            case COUNTRY:               
            case COUNTRY_ID:
                //do noting 
                return null;

            case ITEM_MEASURE:
            case ITEM_MEASURE_ID:
                Log.d(TAG, "Hitting insert item measure on contentPovider ..");
                String itemMeasureName = contentValues.getAsString(MarketWatch.ItemMeasure.NAME);
                String itemMeasureserverId= contentValues.getAsString(MarketWatch.ItemMeasure.SERVER_ID);
                
                ItemMeasure itemMeasure = new ItemMeasure();
                itemMeasure.setItemName(itemMeasureName);
                itemMeasure.setServerId(itemMeasureserverId); 
                long itemMesureId = this.dbManager.insertItemMeasure(itemMeasure); 
                return Uri.withAppendedPath(MarketWatch.Item.CONTENT_URI, "" + itemMesureId);     
                //return MarketWatch.ItemMeasure.CONTENT_ITEM_TYPE;
            case LOCATION:                
            case LOCATION_ID:
                Log.d(TAG, "Hitting insert item location on contentPovider ..");
                String state = contentValues.getAsString(MarketWatch.Location.STATE);
                String county= contentValues.getAsString(MarketWatch.Location.COUNTY);                
                String market= contentValues.getAsString(MarketWatch.Location.MARKET);
                String locationServerId= contentValues.getAsString(MarketWatch.Location.SERVER_ID);
                
                Location location = new Location();
                location.setStateName(state);
                location.setServerId(locationServerId);                
                location.setCounty(county);
                location.setMarket(market);
                
                long locationID = this.dbManager.insertLocation(location); 
                return Uri.withAppendedPath(MarketWatch.Location.CONTENT_URI, "" + locationID); 
                //return MarketWatch.Location.CONTENT_ITEM_TYPE;

            case ITEM_PRICE:
                //return MarketWatch.ItemPrice.CONTENT_TYPE;

            case ITEM_PRICE_ID:
                //return MarketWatch.ItemPrice.CONTENT_ITEM_TYPE;

            case PRICE_REQUEST:
            case PRICE_REQUEST_ID:
                //We don't pull price requests .. retune none
                return null;
                
            case PRICE_RESPONSE:
            case PRICE_RESPONSE_ID:
                Log.d(TAG, "Hitting insert price response on contentPovider ..");
                String requestId = contentValues.getAsString(MarketWatch.PriceResponse.REQUEST_ID);
                String message= contentValues.getAsString(MarketWatch.PriceResponse.MESSAGE);
                
                
                //PriceResponse priceResponse = new PriceResponse();
                //priceResponse.setRequest(null);
                //priceResponse.setMessage(message); 

                long responseId = this.dbManager.insertPriceResponse(requestId, message); 
                return Uri.withAppendedPath(MarketWatch.PriceResponse.CONTENT_URI, "" + responseId); 
                //return MarketWatch.PriceResponse.CONTENT_ITEM_TYPE;

            default:
                throw new RuntimeException("No content provider URI match.");
        }
       
    }

    @Override
    public int delete(Uri arg0, String arg1, String[] arg2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(Uri arg0, ContentValues arg1, String arg2, String[] arg3) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

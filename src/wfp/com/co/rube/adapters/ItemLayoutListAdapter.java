/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import wfp.com.co.rube.R;
import wfp.com.co.rube.entities.Item;

/**
 *
 * @author rube
 */
public class ItemLayoutListAdapter extends BaseAdapter {

	private List<Item> listData = new ArrayList<Item>();

	private LayoutInflater layoutInflater;

	public ItemLayoutListAdapter(Context context, List listData) {
		this.listData = listData;
		layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.item_list_layout_item, null);
			holder = new ViewHolder();
			holder.itemName = (TextView) convertView.findViewById(R.id.item_list_layout_item_name);
			holder.createDate = (TextView) convertView.findViewById(R.id.item_list_layout_item_date);
		
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.itemName.setText(listData.get(position).getItemName());
		holder.createDate.setText(listData.get(position).getCreateDate());
		
		return convertView;
	}

	static class ViewHolder {
		TextView itemName;
		TextView createDate;
	}

}
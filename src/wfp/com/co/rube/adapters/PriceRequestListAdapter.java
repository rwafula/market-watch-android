/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import wfp.com.co.rube.R;
import wfp.com.co.rube.entities.PriceRequest;

/**
 *
 * @author rube
 */
public class PriceRequestListAdapter extends BaseAdapter {

	private List<PriceRequest> listData = new ArrayList<PriceRequest>();

	private LayoutInflater layoutInflater;

	public PriceRequestListAdapter(Context context, List listData) {
		this.listData = listData;
		layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.price_request_list_layout_item, null);
			holder = new ViewHolder();
			holder.itemName = (TextView) convertView.findViewById(R.id.price_request_layout_item_name);
			holder.createDate = (TextView) convertView.findViewById(R.id.price_request_layout_item_date);
                        holder.location = (TextView) convertView.findViewById(R.id.price_request_layout_location);
                        holder.measure = (TextView) convertView.findViewById(R.id.price_request_layout_measure);
		
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.itemName.setText("Item Name: "+listData.get(position).getItemName());
		holder.createDate.setText("Date Created: "+listData.get(position).getCreateDate());
                holder.location.setText("Location: "+listData.get(position).getLocation());
		holder.measure.setText("Measure: "+listData.get(position).getMeasure());
		
		return convertView;
	}

	static class ViewHolder {
		TextView itemName;
		TextView createDate;
                TextView location;
                TextView measure;
	}

}
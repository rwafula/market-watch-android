/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import wfp.com.co.rube.entities.ItemPrice;
import wfp.com.co.rube.R;

/**
 *
 * @author rube
 */
public class ItemPriceListAdapter extends BaseAdapter {

	private ArrayList<ItemPrice> listData = new ArrayList<ItemPrice>();

	private LayoutInflater layoutInflater;

	public ItemPriceListAdapter(Context context, ArrayList listData) {
		this.listData = listData;
		layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.item_row, null);
			holder = new ViewHolder();
			holder.itemName = (TextView) convertView.findViewById(R.id.item_name);
			holder.locationName = (TextView) convertView.findViewById(R.id.location);
			holder.price = (TextView) convertView.findViewById(R.id.item_price);
                        holder.priceDate = (TextView) convertView.findViewById(R.id.date);
                        holder.measure = (TextView) convertView.findViewById(R.id.item_price_measure);
                        holder.isSynced = (TextView) convertView.findViewById(R.id.is_synced);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.itemName.setText("Item: "+ listData.get(position).getItemName());
		holder.locationName.setText("Location Market: "+ listData.get(position).getLocation());
		holder.price.setText("Price: " +listData.get(position).getPrice());
                holder.priceDate.setText("Date Created:" + listData.get(position).getDate());
                holder.measure.setText("Measure: " + listData.get(position).getMeasure());
                holder.measure.setText("Server Updated: " + listData.get(position).getIsSynced());
                
		return convertView;
	}

	static class ViewHolder {
		TextView itemName;
		TextView locationName;
		TextView price;
                TextView priceDate;
                TextView measure;
                TextView isSynced;
	}

}
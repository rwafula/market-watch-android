/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import wfp.com.co.rube.R;
import wfp.com.co.rube.entities.Country;

/**
 *
 * @author rube
 */
public class CountryListAdapter extends BaseAdapter {
    
    private List<Country> listData = new ArrayList<Country>();
    private LayoutInflater layoutInflater;

    public CountryListAdapter(Context context, List listData) {
            this.listData = listData;
            layoutInflater = LayoutInflater.from(context);
    }

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
        
        /**
	 * The drawable image name has the format "flag_$countryCode". We need to
	 * load the drawable dynamically from country code. Code from
	 * http://stackoverflow.com/
	 * questions/3042961/how-can-i-get-the-resource-id-of
	 * -an-image-if-i-know-its-name
	 * 
	 * @param drawableName
	 * @return
	 */
	private int getResId(String drawableName) {

		try {
			Class<R.drawable> res = R.drawable.class;
			Field field = res.getField(drawableName);
			int drawableId = field.getInt(null);
			return drawableId;
		} catch (Exception e) {
			Log.e("COUNTRYPICKER", "Failure to get drawable id.", e);
		}
		return -1;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
                Country country = listData.get(position);
                //Log.d("Country List adapter", "County " + country);
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.county_list_layout_item, null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.county_list_layout_item_name);
			holder.imageView = (ImageView) convertView.findViewById(R.id.county_list_layout_item_icon);
		
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.name.setText(listData.get(position).getName());
                
                String countryCode = country.getCode().toLowerCase(Locale.ENGLISH);
                //Log.d("Country List adapter", "County code: " + countryCode);
                
                String drawableName = "flag_"+countryCode;
				
		holder.imageView.setImageResource(getResId(drawableName));
		
		return convertView;
	}

	static class ViewHolder {
		TextView name;
                ImageView imageView;
	}

}
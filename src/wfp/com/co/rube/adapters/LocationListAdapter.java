/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import wfp.com.co.rube.R;
import wfp.com.co.rube.entities.Location;

/**
 *
 * @author rube
 */
public class LocationListAdapter extends BaseAdapter{
    
 private List<Location> listData = new ArrayList<Location>();

    private LayoutInflater layoutInflater;

    public LocationListAdapter(Context context, List listData) {
            this.listData = listData;
            layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
            return listData.size();
    }

    @Override
    public Object getItem(int position) {
            return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
            return position;
    }

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.location_list_layout_item, null);
			holder = new ViewHolder();
			holder.state = (TextView) convertView.findViewById(R.id.location_list_layout_state);
			holder.county = (TextView) convertView.findViewById(R.id.location_list_layout_county);
			holder.payam = (TextView) convertView.findViewById(R.id.location_list_layout_payam);
                        
                        holder.market = (TextView) convertView.findViewById(R.id.location_list_layout_market);
                        
                        holder.dateCreated =  (TextView) convertView.findViewById(R.id.location_list_layout_create_date);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.state.setText("State: "+ listData.get(position).getStateName());
		holder.county.setText("County: "+ listData.get(position).getCounty());
		holder.payam.setText("Payam: "+ listData.get(position).getPayam());                
                holder.market.setText("Market: "+ listData.get(position).getMarket());                               
                holder.dateCreated.setText("Date Created: "+ listData.get(position).getDateCreated());
                
		return convertView;
	}

	static class ViewHolder {
            TextView state;
            TextView county;
            TextView payam;
            TextView market;
            TextView dateCreated;

	}

}
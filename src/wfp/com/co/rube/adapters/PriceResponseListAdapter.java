/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import wfp.com.co.rube.R;
import wfp.com.co.rube.entities.PriceResponse;

/**
 *
 * @author rube
 */
public class PriceResponseListAdapter extends BaseAdapter {

	private List<PriceResponse> listData = new ArrayList<PriceResponse>();

	private LayoutInflater layoutInflater;

	public PriceResponseListAdapter(Context context, List listData) {
		this.listData = listData;
		layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.price_response_list_layout_item, null);
			holder = new ViewHolder();
			holder.responseDate = (TextView) convertView.findViewById(R.id.price_response_layout_response_date);
			holder.message = (TextView) convertView.findViewById(R.id.price_response_layout_message);
                        //holder.itemName = (TextView) convertView.findViewById(R.id.price_response_layout_item_name);
                        holder.requestDate = (TextView) convertView.findViewById(R.id.price_response_layout_request_date);
                        //holder.location = (TextView) convertView.findViewById(R.id.price_response_layout_location);
                        //holder.measure = (TextView) convertView.findViewById(R.id.price_response_layout_measure);
		
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.responseDate.setText(listData.get(position).getDateCreated());
		holder.message.setText(listData.get(position).getMessage());
                //holder.itemName.setText(listData.get(position).getRequest().getItemName());
                holder.requestDate.setText(listData.get(position).getRequest().getCreateDate());
                //holder.location.setText(listData.get(position).getRequest().getLocation());
                //holder.measure.setText(listData.get(position).getRequest().getMeasure());
		
		return convertView;
	}

	static class ViewHolder {
                TextView responseDate;
                TextView message;
                TextView itemName;
		TextView requestDate;
                TextView location;
                TextView measure;
	}

}
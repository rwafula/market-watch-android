/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import wfp.com.co.rube.db.DbManager;
import wfp.com.co.rube.entities.ItemMeasure;

/**
 *
 * @author rube
 */
public class CreateItemMeasure extends Fragment {
   private DbManager dbManager;
    private final static String TAG = "wfp.CreateItemMeasure";
    public final static String ITEM_NAME = "Item Measures";
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
      
        View view = inflater.inflate(R.layout.item_measure_list_layout_form, container, false);
        
        final EditText itemNameEditField = (EditText)view.findViewById(R.id.item_measure_name);
        Button submitButton = (Button)view.findViewById(R.id.item_measure_btn_submit);
        
        Button cancelButton = (Button)view.findViewById(R.id.item_measure_btn_cancel);
        
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        submitButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) 
            {
 
                String itemName = itemNameEditField.getEditableText().toString();
                ItemMeasure measure = new ItemMeasure();
                measure.setItemName(itemName);
                dbManager.insertItemMeasure(measure);
                Log.d( TAG, "Item submit done: " + itemName);               
                loadItemMeasureList();
                
            }
        });
        
        cancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) 
            {
                loadItemMeasureList();                
            }
        });
 
        return view;
    }
    
    public void loadItemMeasureList(){
        Fragment fragment = new ItemMeasureList();
        Bundle args = new Bundle();   
        args.putString(ItemMeasureList.ITEM_NAME,"Item Measure List");
        fragment.setArguments(args);     

        FragmentManager fragmentManager = getFragmentManager();        
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
        .commit();
        getActivity().getActionBar().setTitle(ItemMeasureList.ITEM_NAME);
    }
    

}

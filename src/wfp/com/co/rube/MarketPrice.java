package wfp.com.co.rube;

import wfp.com.co.rube.entities.ItemPrice;
import wfp.com.co.rube.adapters.ItemPriceListAdapter;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import wfp.com.co.rube.db.DbManager;

public class MarketPrice extends Fragment
{
    public static final String ITEM_NAME = "Price List";
    private View viewRoot;
    private DbManager dbManager;
    private final String TAG = "wfp.MarketPrice";
    
    private ItemPriceListAdapter itemPriceListAdapter;
    private ListView view;
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
        final Context context;
        context = this.getActivity().getApplicationContext();
       
        
        final LinearLayout layout ;
        layout  = (LinearLayout)inflater.inflate(R.layout.market_price, container, false);

        
        view = (ListView)layout.findViewById(R.id.item_list_view);
        
        final EditText searchBoxField = (EditText) layout.findViewById(R.id.search_input);
        
        searchBoxField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchText = searchBoxField.getEditableText().toString();
                    searchMarketPrices(searchText);
                    
                    return true;
                }
                return false;
            }
        });
        
        searchBoxField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                            int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                            int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                    searchMarketPrices(s.toString());
            }
        });
        
        Button addItemButton = (Button)layout.findViewById(R.id.market_price_add_button);
        addItemButton.setOnClickListener(new View.OnClickListener() { 
            @Override
            public void onClick(View arg0) {
                Fragment fragment = new NewPrice();
                Bundle args = new Bundle();   
                args.putString(NewPrice.ITEM_NAME,"New Price");
                fragment.setArguments(args);                
                FragmentManager fragmentManager = getFragmentManager();        
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                .commit();
                getActivity().getActionBar().setTitle(NewPrice.ITEM_NAME);
                MarketWatch.backButtonEnabled = false;
            }
        });
                
        ArrayList itemDetails = getListData();
         
        itemPriceListAdapter = new ItemPriceListAdapter(context, itemDetails);
        
        view.setAdapter(itemPriceListAdapter);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = view.getItemAtPosition(position);
                        ItemPrice newsData = (ItemPrice) o;
                        Toast.makeText(context, "Selected :" + " " + newsData, Toast.LENGTH_LONG).show();
                }

        });
        
        viewRoot = layout;
        return layout;
    }
    
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewRoot != null) {
            ViewGroup parentViewGroup = (ViewGroup) viewRoot.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }
    
    private void searchMarketPrices(String text){
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        ArrayList<ItemPrice> items = this.dbManager.searchItemPrice(text);
        
        itemPriceListAdapter =  
                new ItemPriceListAdapter(getActivity().getApplicationContext(), items);
        Log.d(TAG, "Notifying list cHANGE ..."+items);
        view.setAdapter(itemPriceListAdapter); 
        view.invalidateViews();
        
        
    }

    private ArrayList<ItemPrice> getListData() {
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        ArrayList<ItemPrice> items = this.dbManager.queryItemPrice(null, null, null, null, null);
       
        return items;
    }

   
}

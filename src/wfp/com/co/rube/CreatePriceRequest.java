/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import wfp.com.co.rube.db.DbManager;
import wfp.com.co.rube.entities.Item;
import wfp.com.co.rube.entities.ItemMeasure;
import wfp.com.co.rube.entities.Location;
import wfp.com.co.rube.entities.PriceRequest;

/**
 *
 * @author rube
 */
public class CreatePriceRequest extends Fragment{
    
    private DbManager dbManager;
    private final static String TAG = "wfp.PriceRequest";
    public final static String ITEM_NAME = "Price Request";
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
        Context context = getActivity().getApplicationContext();
        View view = inflater.inflate(R.layout.new_price_request_form, container, false);
        
        final Spinner itemNameEditField = (Spinner)view.findViewById(R.id.price_request_item_name);
        final Spinner locationEditField = (Spinner)view.findViewById(R.id.price_request_location);
        final Spinner measureEditField = (Spinner)view.findViewById(R.id.price_request_measure);
        
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        ArrayList<String> measureList = getMeasures();
        
        ArrayList<String> locationList = getLocations();
        ArrayList<String> itemList = getItems();
        
        ArrayAdapter<String> measureAdapter = new ArrayAdapter<String>(context,
                R.layout.spinner_item, measureList);
        measureAdapter.setDropDownViewResource(R.layout.spinner_layout);
        
        ArrayAdapter<String> locationAdapter = new ArrayAdapter<String>(context,
                R.layout.spinner_item, locationList);
        locationAdapter.setDropDownViewResource(R.layout.spinner_layout);
        
        ArrayAdapter<String> itemAdapter = new ArrayAdapter<String>(context,
                R.layout.spinner_item, itemList);
        itemAdapter.setDropDownViewResource(R.layout.spinner_layout);
        
        itemNameEditField.setAdapter(itemAdapter);
        
        locationEditField.setAdapter(locationAdapter);
        
        measureEditField.setAdapter(measureAdapter);
        
        Button submitButton = (Button)view.findViewById(R.id.price_request_btn_submit);
        
        Button cancelButton = (Button)view.findViewById(R.id.price_request_btn_cancel);
        
        
        
        submitButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) 
            {
 
                String itemName = itemNameEditField.getSelectedItem().toString();
                String measure = measureEditField.getSelectedItem().toString();
                String location = locationEditField.getSelectedItem().toString();
                
                PriceRequest priceRequest = new PriceRequest();
                
                priceRequest.setCreateDate(
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                priceRequest.setItemName(itemName);
                priceRequest.setLocation(location);
                priceRequest.setMeasure(measure); 
                
                long id = dbManager.insertPriceRequest(priceRequest);
                Log.d( TAG, "Price response submit done: " + id);
                loadPriceRequestLists();
                
            }
        });
        
        cancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) 
            {
                loadPriceRequestLists();                
            }
        });
 
        return view;
    }
    
    public void loadPriceRequestLists(){
        Fragment fragment = new PriceRequestList();
        Bundle args = new Bundle();   
        args.putString(PriceRequestList.ITEM_NAME,"Price Request");
        fragment.setArguments(args);     

        FragmentManager fragmentManager = getFragmentManager();        
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
        .commit();
        getActivity().getActionBar().setTitle(PriceRequestList.ITEM_NAME);
    }
    
    public ArrayList<String> getMeasures(){
        
        ArrayList<ItemMeasure> measures 
                = this.dbManager.queryMeasure(null, null, null, null, null);
        
        ArrayList<String> measureList = new ArrayList<String>();
        for(ItemMeasure measure: measures){
            measureList.add(measure.getItemName());
        }
        
        return measureList;
    
    }
    
    public ArrayList<String> getLocations(){
        
        List<Location> locations
                = this.dbManager.queryLocation(null, null, null, null, null);
        
        ArrayList<String> locationList = new ArrayList<String>();
        for(Location location: locations){                         
            locationList.add(location.getMarket());
        }
        
        return locationList;    
    }
    
    public ArrayList<String> getItems(){
        
        List<Item> items
                = this.dbManager.queryItem(null, null, null, null, null);
        
        ArrayList<String> itemList = new ArrayList<String>();
        for(Item item: items){                         
            itemList.add(item.getItemName());
        }
        
        Log.d(TAG, itemList.toString());
        return itemList;    
    }
    
    
    

}

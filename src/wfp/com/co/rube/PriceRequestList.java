/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import wfp.com.co.rube.adapters.PriceRequestListAdapter;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import wfp.com.co.rube.db.DbManager;
import wfp.com.co.rube.entities.PriceRequest;

/**
 *
 * @author rube
 */
public class PriceRequestList extends Fragment{
    
    public static final String ITEM_NAME = "Price Request";
    private final String TAG = "wfp.PriceRequestList";
    private View viewRoot;
    private DbManager dbManager;
    
    private PriceRequestListAdapter priceRequestListAdapter;
    private ListView view;
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
        final Context context;
        context = this.getActivity().getApplicationContext();
        
        final LinearLayout layout ;
        layout  = (LinearLayout)inflater.inflate(R.layout.price_request_list_layout, container, false);

        
        view = (ListView)layout.findViewById(R.id.price_request_list_view);
        
        Button addItemButton = (Button)layout.findViewById(R.id.price_request_add_button);
        addItemButton.setOnClickListener(new View.OnClickListener() { 
            @Override
            public void onClick(View arg0) {
                Fragment fragment = new CreatePriceRequest();
                Bundle args = new Bundle();   
                args.putString(CreatePriceRequest.ITEM_NAME,"Price Request");
                fragment.setArguments(args);                
                FragmentManager fragmentManager = getFragmentManager();        
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
                .commit();
                getActivity().getActionBar().setTitle(CreatePriceRequest.ITEM_NAME);
                MarketWatch.backButtonEnabled = false;
            }
        });
           
        final EditText searchBoxField = (EditText) layout.findViewById(R.id.search_price_request_input);
        
        searchBoxField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchText = searchBoxField.getEditableText().toString();
                    searchPriceRequestList(searchText);
                    
                    return true;
                }
                return false;
            }
        });
        
        searchBoxField.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                            int count) {
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                            int after) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                    searchPriceRequestList(s.toString());
            }
        });
                
        List<PriceRequest> itemDetails = getListData();
        priceRequestListAdapter = new PriceRequestListAdapter(context, itemDetails);
        view.setAdapter(priceRequestListAdapter);
        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = view.getItemAtPosition(position);
                        PriceRequest itemData = (PriceRequest) o;
                        Toast.makeText(context, "Selected :" + " " + itemData, Toast.LENGTH_LONG).show();
                }

        });
        
        viewRoot = layout;
        return layout;
    }
    
    
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (viewRoot != null) {
            ViewGroup parentViewGroup = (ViewGroup) viewRoot.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    private List<PriceRequest> getListData() {
        
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        return dbManager.queryPriceRequest(null, null,null, null, null);

    }  
    
    
    private void searchPriceRequestList(String text){
         if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        List<PriceRequest> priceRequests = this.dbManager.searchPriceRequests(text);
        
        priceRequestListAdapter =  
                new PriceRequestListAdapter(getActivity().getApplicationContext(), priceRequests);
        Log.d(TAG, "Notifying list cHANGE ..."+priceRequests);
        view.setAdapter(priceRequestListAdapter); 
        view.invalidateViews();
    
    }
    
}

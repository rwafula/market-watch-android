/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.db;

import java.util.Date;

import wfp.com.co.rube.db.MarketWatch.Country;
import wfp.com.co.rube.db.MarketWatch.CountryColumns;

import wfp.com.co.rube.db.MarketWatch.Item;
import wfp.com.co.rube.db.MarketWatch.ItemColumns;

import wfp.com.co.rube.db.MarketWatch.ItemPrice;
import wfp.com.co.rube.db.MarketWatch.ItemPriceColumns;

import wfp.com.co.rube.db.MarketWatch.Location;
import wfp.com.co.rube.db.MarketWatch.LocationColumns;

import wfp.com.co.rube.db.MarketWatch.ItemMeasure;

import wfp.com.co.rube.db.MarketWatch.PriceRequest;
import wfp.com.co.rube.db.MarketWatch.PriceRequestColumns;

import wfp.com.co.rube.db.MarketWatch.PriceResponse;
import wfp.com.co.rube.db.MarketWatch.PriceResponseColumns;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rube
 *
 */
public class DbManager extends SQLiteOpenHelper {

    private final Context mContext;
    private final static String TAG = "wfp.DbManager";

    public DbManager(Context context) {
        super(context, MarketWatch.DATABASE_NAME, null, MarketWatch.DATABASE_VERSION);
        this.mContext = context;
    }

    /*
     * (non-Javadoc)
     * @see
     * android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite
     * .SQLiteDatabase)
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(Country.CREATE_STATEMENT);
        db.execSQL(Item.CREATE_STATEMENT);
        db.execSQL(ItemPrice.CREATE_STATEMENT);
        db.execSQL(Location.CREATE_STATEMENT);
        db.execSQL(PriceRequest.CREATE_STATEMENT);
        db.execSQL(PriceResponse.CREATE_STATEMENT);
        db.execSQL(ItemMeasure.CREATE_STATEMENT);
        db.execSQL(MarketWatch.Sync.CREATE_STATEMENT);
    }

    /**
     * Will update version 1 through 5 to version 8
     *
     * @param db
     * @param current
     * @param targetVersion
     * @see
     * android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase,
     * int, int)
     * @see GPStracking.DATABASE_VERSION
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int current, int targetVersion) {
        Log.i(TAG, "Nothing to upgrade ... just got called " + current + " to " + targetVersion);

    }

    public void vacuum() {
        new Thread() {
            @Override
            public void run() {
                SQLiteDatabase sqldb = getWritableDatabase();
                sqldb.execSQL("VACUUM");
            }
        }.start();

    }

    /**
     * Creates a country
     *
     * @param name name
     * @param dialCode dial code
     * @param flafFile file path
     * @return
     */
    long insertCountry(String name, String dialCode, String flagFile) {

        SQLiteDatabase sqldb = getWritableDatabase();

        ContentValues args = new ContentValues();
        args.put(CountryColumns.NAME, name);
        args.put(CountryColumns.DIAL_CODE, dialCode);
        args.put(CountryColumns.FLAG_FILE, flagFile);
        String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        args.put(CountryColumns.DATE_CREATED, today);

        long countryId = sqldb.insert(Country.TABLE, null, args);

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(Country.CONTENT_URI, "" + countryId);
        resolver.notifyChange(notifyUri, null);

        Log.d(TAG, "Country Store: " + notifyUri);
        return countryId;
    }

    public List<List> queryCountry(String selection, String[] selectionArgs,
            String groupBy, String having, String orderBy) {
        SQLiteDatabase sqldb = getWritableDatabase();
        Cursor cursor = sqldb.query(Country.TABLE, CountryColumns.COLUMNS,
                selection, selectionArgs, groupBy, having, orderBy);

        List countries = new ArrayList<wfp.com.co.rube.entities.Country>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.Country country = new wfp.com.co.rube.entities.Country();
                country.setName(cursor.getString(cursor.getColumnIndex(CountryColumns.NAME)));
                country.setDialCode(cursor.getString(cursor.getColumnIndex(CountryColumns.DIAL_CODE)));
                countries.add(country);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return countries;

    }

    /**
     * @param countryId
     * @return
     */
    int deleteCountry(long countryId) {
        SQLiteDatabase sqldb = getWritableDatabase();
        int affected = sqldb.delete(Country.TABLE, Country._ID + "= ?",
                new String[]{String.valueOf(countryId)});

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(Country.CONTENT_URI, "" + countryId);
        resolver.notifyChange(notifyUri, null);

        return affected;
    }

    /**
     * Creates a item
     *
     * @param name item name
     * @param serverId
     * @return
     */
    public long insertItem(String name, String serverId) {

        SQLiteDatabase sqldb = getWritableDatabase();

        ContentValues args = new ContentValues();
        args.put(ItemColumns.NAME, name);
        args.put(ItemColumns.SERVER_ID, serverId);
        String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        args.put(ItemColumns.DATE_CREATED, today);

        long itemId = sqldb.insert(Item.TABLE, null, args);

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(Item.CONTENT_URI, "" + itemId);
        resolver.notifyChange(notifyUri, null);

        Log.d(TAG, "Item Stored: " + notifyUri);
        return itemId;
    }
    
    //compatibility with local inserts
    public long insertItem(String name) {
        return insertItem(name, "0");
    }
    public ArrayList<wfp.com.co.rube.entities.Item> searchItems(String text) {
        text = "%" + text + "%";
        SQLiteDatabase sqldb = getWritableDatabase();
        String rawSQl = "Select * from " + Item.TABLE + " i "
                + " where i.name like ?";

        Cursor cursor = sqldb.rawQuery(rawSQl, new String[]{text});

        ArrayList<wfp.com.co.rube.entities.Item> items = new ArrayList<wfp.com.co.rube.entities.Item>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.Item item = new wfp.com.co.rube.entities.Item();
                item.setItemName(cursor.getString(cursor.getColumnIndex(ItemColumns.NAME)));
                item.setCreateDate(cursor.getString(cursor.getColumnIndex(ItemColumns.DATE_CREATED)));
                items.add(item);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return items;

    }

    public ArrayList<wfp.com.co.rube.entities.ItemMeasure> searchItemMeasures(String text) {
        text = "%" + text + "%";
        SQLiteDatabase sqldb = getWritableDatabase();
        String rawSQl = "Select * from " + ItemMeasure.TABLE + " i "
                + " where i.name like ?";

        Cursor cursor = sqldb.rawQuery(rawSQl, new String[]{text});

        ArrayList<wfp.com.co.rube.entities.ItemMeasure> items = new ArrayList<wfp.com.co.rube.entities.ItemMeasure>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.ItemMeasure measure = new wfp.com.co.rube.entities.ItemMeasure();
                measure.setItemName(cursor.getString(cursor.getColumnIndex(MarketWatch.ItemMeasureColumns.NAME)));
                measure.setCreateDate(cursor.getString(cursor.getColumnIndex(MarketWatch.ItemMeasureColumns.DATE_CREATED)));
                items.add(measure);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return items;

    }

    public List<wfp.com.co.rube.entities.Item> queryItem(String selection, String[] selectionArgs,
            String groupBy, String having, String orderBy) {
        SQLiteDatabase sqldb = getWritableDatabase();
        Cursor cursor = sqldb.query(Item.TABLE, ItemColumns.COLUMNS,
                selection, selectionArgs, groupBy, having, orderBy);

        List items = new ArrayList<wfp.com.co.rube.entities.Item>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.Item item = new wfp.com.co.rube.entities.Item();
                item.setItemName(cursor.getString(cursor.getColumnIndex(ItemColumns.NAME)));
                item.setCreateDate(cursor.getString(cursor.getColumnIndex(ItemColumns.DATE_CREATED)));
                items.add(item);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return items;

    }

    public ArrayList<wfp.com.co.rube.entities.ItemMeasure> queryMeasure(String selection, String[] selectionArgs,
            String groupBy, String having, String orderBy) {
        SQLiteDatabase sqldb = getWritableDatabase();
        Cursor cursor = sqldb.query(ItemMeasure.TABLE, MarketWatch.ItemMeasureColumns.COLUMNS,
                selection, selectionArgs, groupBy, having, orderBy);

        ArrayList<wfp.com.co.rube.entities.ItemMeasure> items = new ArrayList<wfp.com.co.rube.entities.ItemMeasure>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.ItemMeasure itemMeasure = new wfp.com.co.rube.entities.ItemMeasure();
                itemMeasure.setItemName(cursor.getString(cursor.getColumnIndex(MarketWatch.ItemMeasureColumns.NAME)));
                itemMeasure.setCreateDate(cursor.getString(cursor.getColumnIndex(MarketWatch.ItemMeasureColumns.DATE_CREATED)));
                items.add(itemMeasure);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return items;

    }

    public long getOrInsertCountry(String countryName) {
        SQLiteDatabase sqldb = getWritableDatabase();
        String COUNTRY_QUERY = " SELECT * FROM " + Country.TABLE + " a "
                + " where name = ? ";
        long countryId = 0;

        Cursor cursor = sqldb.rawQuery(COUNTRY_QUERY, new String[]{countryName});
        if (cursor != null && cursor.moveToFirst()) {
            if (!cursor.isAfterLast()) {
                countryId = cursor.getLong(cursor.getColumnIndex(Country._ID));
                return countryId;
            }
        }

        ContentValues args = new ContentValues();
        args.put(Country.NAME, countryName);
        args.put(Country.DATE_CREATED,
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

        countryId = sqldb.insert(Country.TABLE, null, args);

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(Country.CONTENT_URI, "" + countryId);
        resolver.notifyChange(notifyUri, null);

        Log.d(TAG, "Country Stored: " + notifyUri);

        return countryId;
    }

    public long insertLocation(wfp.com.co.rube.entities.Location newLocation) {

        long countryId = getOrInsertCountry("South Sudan");

        SQLiteDatabase sqldb = getWritableDatabase();

        ContentValues args = new ContentValues();
        args.put(Location.COUNTRY_ID, countryId);
        args.put(Location.STATE, newLocation.getStateName());
        args.put(Location.COUNTY, newLocation.getCounty());        
        args.put(Location.MARKET, newLocation.getMarket());
        String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        args.put(Location.DATE_CREATED, today);

        long locationId = sqldb.insert(Location.TABLE, null, args);

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(Location.CONTENT_URI, "" + locationId);
        resolver.notifyChange(notifyUri, null);

        Log.d(TAG, "Item Stored: " + notifyUri);
        return locationId;

    }

    public long insertItemMeasure(wfp.com.co.rube.entities.ItemMeasure measure) {

        SQLiteDatabase sqldb = getWritableDatabase();

        ContentValues args = new ContentValues();
        args.put(MarketWatch.ItemMeasureColumns.NAME, measure.getItemName());
        String severId = measure.getServerId()==null? "0" :measure.getServerId();
        
        args.put(MarketWatch.ItemMeasureColumns.SERVER_ID, severId);
        String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        args.put(MarketWatch.ItemMeasureColumns.DATE_CREATED, today);

        long measureId = sqldb.insert(ItemMeasure.TABLE, null, args);

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(Location.CONTENT_URI, "" + measureId);
        resolver.notifyChange(notifyUri, null);

        Log.d(TAG, "Item Stored: " + notifyUri);

        return measureId;

    }

    public ArrayList<wfp.com.co.rube.entities.ItemMeasure> queryItemMeasure(
            String selection, String[] selectionArgs,
            String groupBy, String having, String orderBy) {

        SQLiteDatabase sqldb = getWritableDatabase();
        Cursor cursor = sqldb.query(ItemMeasure.TABLE, MarketWatch.ItemMeasureColumns.COLUMNS,
                selection, selectionArgs, groupBy, having, orderBy);

        ArrayList<wfp.com.co.rube.entities.ItemMeasure> itemMeasures = new ArrayList<wfp.com.co.rube.entities.ItemMeasure>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.ItemMeasure ItemMeasure = new wfp.com.co.rube.entities.ItemMeasure();
                ItemMeasure.setItemName(cursor.getString(cursor.getColumnIndex(MarketWatch.ItemMeasureColumns.NAME)));
                ItemMeasure.setCreateDate(cursor.getString(cursor.getColumnIndex(MarketWatch.ItemMeasureColumns.DATE_CREATED)));
                itemMeasures.add(ItemMeasure);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return itemMeasures;

    }

    public List<wfp.com.co.rube.entities.Location> searchLocations(String text){
        text = "%"+text+"%";
        SQLiteDatabase sqldb = getWritableDatabase();

        String LOCATIONS_QUERY = " SELECT * FROM " + Location.TABLE + " a "
                + " where state like ? or market like ? or "
                + " county like ?";

        Cursor cursor = sqldb.rawQuery(LOCATIONS_QUERY, new String[]{text, text, text});

        List locations = new ArrayList<wfp.com.co.rube.entities.Location>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.Location location = new wfp.com.co.rube.entities.Location();
                location.setStateName(cursor.getString(cursor.getColumnIndex(Location.STATE)));
                location.setCounty(cursor.getString(cursor.getColumnIndex(Location.COUNTY)));                
                location.setMarket(cursor.getString(cursor.getColumnIndex(LocationColumns.MARKET)));
                location.setDateCreated(cursor.getString(cursor.getColumnIndex(LocationColumns.DATE_CREATED)));                
                locations.add(location);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return locations;
    
    }
    
    public List<wfp.com.co.rube.entities.Location> queryLocation(String selection, String[] selectionArgs,
            String groupBy, String having, String orderBy) {
        SQLiteDatabase sqldb = getWritableDatabase();

        Cursor cursor = sqldb.query(Location.TABLE, MarketWatch.Location.COLUMNS,
                selection, selectionArgs, groupBy, having, orderBy);
       
        List locations = new ArrayList<wfp.com.co.rube.entities.Location>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.Location location = new wfp.com.co.rube.entities.Location();
                location.setStateName(cursor.getString(cursor.getColumnIndex(Location.STATE)));
                location.setCounty(cursor.getString(cursor.getColumnIndex(Location.COUNTY)));                               
                location.setMarket(cursor.getString(cursor.getColumnIndex(LocationColumns.MARKET)));
                location.setDateCreated(cursor.getString(cursor.getColumnIndex(LocationColumns.DATE_CREATED)));                
                locations.add(location);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return locations;

    }

    /**
     * Creates a item price
     *
     * @param itemId item name reference
     * @param locationId item location reference
     * @param price item price
     * @param createdBy item creator
     * @return
     */
    long insertItemPrice(String itemId, String locationId, double price,
            String createdBy) {

        SQLiteDatabase sqldb = getWritableDatabase();

        ContentValues args = new ContentValues();
        args.put(ItemPriceColumns.ITEM_ID, itemId);
        args.put(ItemPriceColumns.LOCATION_ID, locationId);
        args.put(ItemPriceColumns.PRICE, "" + price);
        args.put(ItemPriceColumns.CREATED_BY, "" + createdBy);

        String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        args.put(ItemPriceColumns.DATE_CREATED, today);
        long itemPriceId = sqldb.insert(ItemPrice.TABLE, null, args);

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(ItemPrice.CONTENT_URI, "" + itemPriceId);
        resolver.notifyChange(notifyUri, null);

        Log.d(TAG, "Item item price Stored: " + notifyUri);
        return itemPriceId;
    }

    /**
     * Creates a location
     *
     * @param countyId owning country
     * @param city city
     * @param town town
     * @param estate village or estate
     * @param market individual market
     * @param province location province
     * @return
     */
    long insertLocation(String countyId, String state, String county,
             String market) {

        SQLiteDatabase sqldb = getWritableDatabase();

        ContentValues args = new ContentValues();
        args.put(LocationColumns.COUNTRY_ID, countyId);
        args.put(LocationColumns.STATE, state);
        args.put(LocationColumns.COUNTY, county);      
        args.put(LocationColumns.MARKET, market);


        String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        args.put(LocationColumns.DATE_CREATED, today);
        long locationId = sqldb.insert(Location.TABLE, null, args);

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(Location.CONTENT_URI, "" + locationId);
        resolver.notifyChange(notifyUri, null);

        Log.d(TAG, "Item location Stored: " + notifyUri);
        return locationId;
    }

    long getLocationId(String market) {

        String sql = "select _id from " + Location.TABLE + " where market = ?";
        SQLiteDatabase sqldb = getWritableDatabase();
        Cursor cursor = sqldb.rawQuery(sql, new String[]{market});

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                return cursor.getLong(cursor.getColumnIndex(Location._ID));
            }
        }
        return 0;
    }

    long getMeasureId(String name) {

        String sql = "select _id from " + ItemMeasure.TABLE + " where name = ?";
        SQLiteDatabase sqldb = getWritableDatabase();
        Cursor cursor = sqldb.rawQuery(sql, new String[]{name});

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                return cursor.getLong(cursor.getColumnIndex(ItemMeasure._ID));
            }
        }
        return 0;
    }

    long getItemId(String name) {

        String sql = "select _id from " + Item.TABLE + " where name = ?";
        SQLiteDatabase sqldb = getWritableDatabase();
        Cursor cursor = sqldb.rawQuery(sql, new String[]{name});

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                return cursor.getLong(cursor.getColumnIndex(Item._ID));
            }
        }
        return 0;
    }

    long getCountryId(String locationId) {

        String sql = "select " + Location.COUNTRY_ID + " from " + Location.TABLE + " where _id = ?";
        SQLiteDatabase sqldb = getWritableDatabase();
        Cursor cursor = sqldb.rawQuery(sql, new String[]{locationId});

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                return cursor.getLong(cursor.getColumnIndex(Location.COUNTRY_ID));
            }
        }
        return 0;
    }

    /**
     * Creates a price request
     *
     * @param newPriceRequest
     * @return
     */
    public long insertPriceRequest(wfp.com.co.rube.entities.PriceRequest newPriceRequest) {

        SQLiteDatabase sqldb = getWritableDatabase();
        long itemId = getItemId(newPriceRequest.getItemName());
        long measureId = getMeasureId(newPriceRequest.getMeasure());
        long locationId = getLocationId(newPriceRequest.getLocation());
        long countryId = getCountryId("" + locationId);

        ContentValues args = new ContentValues();
        args.put(PriceRequestColumns.COUNTRY_ID, countryId);
        args.put(PriceRequestColumns.USER_ID, 0);
        args.put(PriceRequestColumns.ORGANIZATION_ID, 0);
        args.put(PriceRequestColumns.ITEM_ID, itemId);
        args.put(PriceRequestColumns.LOCATION_ID, locationId);
        args.put(PriceRequestColumns.MEASURE_ID, measureId);
        args.put(PriceRequestColumns.DATE_CREATED,
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

        long requestId = sqldb.insert(PriceRequest.TABLE, null, args);

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(PriceRequest.CONTENT_URI, "" + requestId);
        resolver.notifyChange(notifyUri, null);

        Log.d(TAG, "Item price request Stored: " + notifyUri);

        return requestId;
    }

    public long insertNewPrice(wfp.com.co.rube.entities.ItemPrice newItemPrice) {
        SQLiteDatabase sqldb = getWritableDatabase();
        ContentValues args = new ContentValues();

        long itemId = getItemId(newItemPrice.getItemName());
        long locationId = getLocationId(newItemPrice.getLocation());
        long measureId = getMeasureId(newItemPrice.getMeasure());

        args.put(ItemPrice.ITEM_ID, itemId);
        args.put(ItemPrice.LOCATION_ID, locationId);
        args.put(ItemPrice.PRICE, newItemPrice.getPrice());
        args.put(ItemPrice.APPROVED, false);
        args.put(ItemPrice.CREATED_BY, 0);
        args.put(ItemPrice.CREATED_BY, 0);
        args.put(ItemPrice.MEASURE_ID, measureId);
        args.put(ItemPrice.DATE_CREATED,
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

        long priceId = sqldb.insert(ItemPrice.TABLE, null, args);

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(ItemPrice.CONTENT_URI, "" + priceId);
        resolver.notifyChange(notifyUri, null);

        Log.d(TAG, "Item price Stored: " + notifyUri);

        return priceId;

    }

    public ArrayList<wfp.com.co.rube.entities.ItemPrice> searchItemPrice(String text) {
        text = "%" + text + "%";
        SQLiteDatabase sqldb = getWritableDatabase();
        String rawSQl = "Select l.market, p.date_created, i.name as item, "
                + " im.name as item_measure, p.price, p.server_id "
                + " from " + ItemPrice.TABLE + " p "
                + " INNER JOIN " + Item.TABLE + " i on i._id = p.item_id "
                + " INNER JOIN " + ItemMeasure.TABLE + " im on im._id = p.measure_id"
                + " INNER JOIN " + Location.TABLE + " l on l._id = p.location_id "
                + " where l.market like ? or i.name like ?";

        Cursor cursor = sqldb.rawQuery(rawSQl, new String[]{text, text});

        ArrayList<wfp.com.co.rube.entities.ItemPrice> itemPrices = new ArrayList<wfp.com.co.rube.entities.ItemPrice>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.ItemPrice itemPrice = new wfp.com.co.rube.entities.ItemPrice();
                itemPrice.setLocation(cursor.getString(cursor.getColumnIndex(LocationColumns.MARKET)));
                itemPrice.setDate(cursor.getString(cursor.getColumnIndex("date_created")));
                itemPrice.setMeasure(cursor.getString(cursor.getColumnIndex("item_measure")));
                itemPrice.setItemName(cursor.getString(cursor.getColumnIndex("item")));
                itemPrice.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                itemPrice.setIsSynced(cursor.getLong(cursor.getColumnIndex("server_id"))> 0 ?"True":"False");
                itemPrices.add(itemPrice);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return itemPrices;
    }

    public ArrayList<wfp.com.co.rube.entities.ItemPrice> queryItemPrice(String selection, String[] selectionArgs,
            String groupBy, String having, String orderBy) {
        SQLiteDatabase sqldb = getWritableDatabase();
        String rawSQl = "Select l.market, p.date_created, i.name as item, "
                + " im.name as item_measure, p.price, p.server_id "
                + " from " + ItemPrice.TABLE + " p "
                + " INNER JOIN " + Item.TABLE + " i on i._id = p.item_id "
                + " INNER JOIN " + ItemMeasure.TABLE + " im on im._id = p.measure_id"
                + " INNER JOIN " + Location.TABLE + " l on l._id = p.location_id ";

        Cursor cursor = sqldb.rawQuery(rawSQl, null);

        ArrayList<wfp.com.co.rube.entities.ItemPrice> itemPrices = new ArrayList<wfp.com.co.rube.entities.ItemPrice>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.ItemPrice itemPrice = new wfp.com.co.rube.entities.ItemPrice();
                itemPrice.setLocation(cursor.getString(cursor.getColumnIndex(LocationColumns.MARKET)));
                itemPrice.setDate(cursor.getString(cursor.getColumnIndex("date_created")));
                itemPrice.setMeasure(cursor.getString(cursor.getColumnIndex("item_measure")));
                itemPrice.setItemName(cursor.getString(cursor.getColumnIndex("item")));
                itemPrice.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                itemPrice.setIsSynced(cursor.getLong(cursor.getColumnIndex("server_id"))> 0 ?"True":"False");
                itemPrices.add(itemPrice);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return itemPrices;

    }

    public List<wfp.com.co.rube.entities.PriceRequest> searchPriceRequests(String text) {
        text = "%" + text + "%";
        SQLiteDatabase sqldb = getWritableDatabase();
        String rawSQl = "Select l.market, p.date_created, i.name as item, "
                + " im.name as item_measure "
                + " from " + PriceRequest.TABLE + " p "
                + " INNER JOIN " + Item.TABLE + " i on i._id = p.item_id "
                + " INNER JOIN " + ItemMeasure.TABLE + " im on im._id = p.measure_id"
                + " INNER JOIN " + Location.TABLE + " l on l._id = p.location_id "
                + " where l.market like ? or i.name like ?";

        Cursor cursor = sqldb.rawQuery(rawSQl, new String[]{text, text});

        ArrayList<wfp.com.co.rube.entities.PriceRequest> priceRequests = new ArrayList<wfp.com.co.rube.entities.PriceRequest>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.PriceRequest priceRequest = new wfp.com.co.rube.entities.PriceRequest();
                priceRequest.setLocation(cursor.getString(cursor.getColumnIndex(LocationColumns.MARKET)));
                priceRequest.setCreateDate(cursor.getString(cursor.getColumnIndex("date_created")));
                priceRequest.setMeasure(cursor.getString(cursor.getColumnIndex("item_measure")));
                priceRequest.setItemName(cursor.getString(cursor.getColumnIndex("item")));
                priceRequests.add(priceRequest);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return priceRequests;

    }

    public List<wfp.com.co.rube.entities.PriceRequest> queryPriceRequest(String selection, String[] selectionArgs,
            String groupBy, String having, String orderBy) {
        SQLiteDatabase sqldb = getWritableDatabase();
        String rawSQl = "Select l.market, p.date_created, i.name as item, "
                + " im.name as item_measure "
                + " from " + PriceRequest.TABLE + " p "
                + " INNER JOIN " + Item.TABLE + " i on i._id = p.item_id "
                + " INNER JOIN " + ItemMeasure.TABLE + " im on im._id = p.measure_id"
                + " INNER JOIN " + Location.TABLE + " l on l._id = p.location_id ";
        
        if(selection != null){
            rawSQl += " where "+ selection;
        }

        Cursor cursor = sqldb.rawQuery(rawSQl, selectionArgs);

        ArrayList<wfp.com.co.rube.entities.PriceRequest> priceRequests = new ArrayList<wfp.com.co.rube.entities.PriceRequest>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.PriceRequest priceRequest = new wfp.com.co.rube.entities.PriceRequest();
                priceRequest.setLocation(cursor.getString(cursor.getColumnIndex(LocationColumns.MARKET)));
                priceRequest.setCreateDate(cursor.getString(cursor.getColumnIndex("date_created")));
                priceRequest.setMeasure(cursor.getString(cursor.getColumnIndex("item_measure")));
                priceRequest.setItemName(cursor.getString(cursor.getColumnIndex("item")));
                priceRequests.add(priceRequest);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return priceRequests;
    }

    public List<wfp.com.co.rube.entities.PriceResponse> searchPriceResponses(String text) {
        text = "%" + text + "%";
        SQLiteDatabase sqldb = getWritableDatabase();

        String RESPONSES_QUERY = " SELECT a.*, b.date_created request_date,"
                + " c.name as item_name, e.name as measure, d.city as city, "
                + " d.market as market, d.village_estate as estate"
                + " FROM " + PriceResponse.TABLE + " a "
                + " LEFT JOIN " + PriceRequest.TABLE + " b ON a.request_id=b._id "
                + " INNER JOIN " + Location.TABLE + " d on d._id = b.location_id "
                + " INNER JOIN " + Item.TABLE + " c on c._id = b.item_id "
                + " LEFT JOIN " + ItemMeasure.TABLE + " e on e._id = b.measure_id "
                + " where c.name like ? or d.market like ?";

        Cursor cursor = sqldb.rawQuery(RESPONSES_QUERY, new String[]{text, text});

        ArrayList<wfp.com.co.rube.entities.PriceResponse> priceResponses = new ArrayList<wfp.com.co.rube.entities.PriceResponse>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.PriceResponse priceResponse = new wfp.com.co.rube.entities.PriceResponse();
                wfp.com.co.rube.entities.PriceRequest priceRequest = new wfp.com.co.rube.entities.PriceRequest();

                priceRequest.setItemName(cursor.getString(cursor.getColumnIndex("item_name")));
                priceRequest.setMeasure(cursor.getString(cursor.getColumnIndex("measure")));

                String location = cursor.getString(cursor.getColumnIndex("city"))
                        + cursor.getString(cursor.getColumnIndex("estate"))
                        + cursor.getString(cursor.getColumnIndex("market"));
                priceRequest.setLocation(location);
                priceRequest.setCreateDate(cursor.getString(cursor.getColumnIndex("request_date")));

                priceResponse.setRequest(priceRequest);

                priceResponse.setMessage(cursor.getString(cursor.getColumnIndex(PriceResponseColumns.MESSAGE)));
                priceResponse.setDateCreated(cursor.getString(cursor.getColumnIndex("a.date_created")));
                priceResponses.add(priceResponse);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return priceResponses;

    }

    public List<wfp.com.co.rube.entities.PriceResponse> queryPriceResponse(String selection, String[] selectionArgs,
            String groupBy, String having, String orderBy) {
        SQLiteDatabase sqldb = getWritableDatabase();

        String RESPONSES_QUERY = " SELECT a.*, b.date_created request_date,"
                + " c.name as item_name, e.name as measure, d.county as county, "
                + " d.market as market "
                + " FROM " + PriceResponse.TABLE + " a "
                + " LEFT JOIN " + PriceRequest.TABLE + " b ON a.request_id=b._id "
                + " LEFT JOIN " + Location.TABLE + " d on d._id = b.location_id "
                + " LEFT JOIN " + Item.TABLE + " c on c._id = b.item_id "
                + " LEFT JOIN " + ItemMeasure.TABLE + " e on e._id = b.measure_id";
        
        if(selection != null){
            RESPONSES_QUERY += " where "+selection;
        }

        Cursor cursor = sqldb.rawQuery(RESPONSES_QUERY, selectionArgs);

        ArrayList<wfp.com.co.rube.entities.PriceResponse> priceResponses = new ArrayList<wfp.com.co.rube.entities.PriceResponse>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                wfp.com.co.rube.entities.PriceResponse priceResponse = new wfp.com.co.rube.entities.PriceResponse();
                wfp.com.co.rube.entities.PriceRequest priceRequest = new wfp.com.co.rube.entities.PriceRequest();

                priceRequest.setItemName(cursor.getString(cursor.getColumnIndex("item_name")));
                priceRequest.setMeasure(cursor.getString(cursor.getColumnIndex("measure")));

                String location = cursor.getString(cursor.getColumnIndex("county"))                        
                        + cursor.getString(cursor.getColumnIndex("market"));
                priceRequest.setLocation(location);
                priceRequest.setCreateDate(cursor.getString(cursor.getColumnIndex("request_date")));

                priceResponse.setRequest(priceRequest);

                priceResponse.setMessage(cursor.getString(cursor.getColumnIndex(PriceResponseColumns.MESSAGE)));
                priceResponse.setDateCreated(
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
                priceResponses.add(priceResponse);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return priceResponses;

    }

    /**
     * Creates a price response
     *
     * @param requestId parent request
     * @param message response message
     * @return
     */
    public long insertPriceResponse(String requestId, String message) {

        SQLiteDatabase sqldb = getWritableDatabase();

        ContentValues args = new ContentValues();
        args.put(PriceResponseColumns.REQUEST_ID, requestId);
        args.put(PriceResponseColumns.MESSAGE, message);

        String today = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        args.put(PriceResponseColumns.DATE_CREATED, today);
        long responseId = sqldb.insert(PriceResponse.TABLE, null, args);

        ContentResolver resolver = this.mContext.getContentResolver();
        Uri notifyUri = Uri.withAppendedPath(PriceResponse.CONTENT_URI, "" + responseId);
        resolver.notifyChange(notifyUri, null);

        Log.d(TAG, "Item response Stored: " + notifyUri);

        return responseId;
    }
    
    public String getLastSyncDate(String table){
        
        SQLiteDatabase sqldb = getWritableDatabase();
        String query = " SELECT date_created  FROM sync_table where item "
                + " = ? order by _id desc limit 1";
        
        Cursor cursor = sqldb.rawQuery(query, new String[]{table});
      
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String date_created = cursor.getString(cursor.getColumnIndex("date_created"));
                Log.d(TAG, "Returning date created "+date_created);
                return date_created;
            }
            cursor.close();
        }
        
        
        Log.d(TAG, "Returning date created null .." + query + " ..." + table);
        return null;
         
    }
    
    public void updateSyncDate(String table, String lastCreated){
        SQLiteDatabase sqldb = getWritableDatabase();
        
        String now =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        ContentValues args = new ContentValues();
        if (lastCreated != null){
            args.put("date_created", now);
            sqldb.update(MarketWatch.Sync.TABLE, args, " item = ? ", new String[]{table});
            
        }else {
            Log.d(TAG, "Running esle in insert sync");
            
            args.put(MarketWatch.Sync.NAME, table);
            args.put(MarketWatch.Sync.DATE_CREATED, now);
            long syncID = sqldb.insert(MarketWatch.Sync.TABLE, null, args);
            
            ContentResolver resolver = this.mContext.getContentResolver();
            Uri notifyUri = Uri.withAppendedPath(MarketWatch.Sync.CONTENT_URI, "" + syncID);
            resolver.notifyChange(notifyUri, null);
        }
        
        
    }
    
    public void updateItemMeasureAsPosted(String serverId, String itemName){
        Log.d(TAG, "Updating item measure as posted to the server");
        SQLiteDatabase sqldb = getWritableDatabase();  
        ContentValues args = new ContentValues();
        args.put("server_id", serverId);
        
        sqldb.update(MarketWatch.ItemMeasure.TABLE, args, " name = ? ", new String[]{itemName});
    
    }
    
    public void updateItemAsPosted(String serverId, String itemName){
        Log.d(TAG, "Updating item as posted to the server");
        SQLiteDatabase sqldb = getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put("server_id", serverId);
        
        sqldb.update(MarketWatch.Item.TABLE, args, " name = ? ", new String[]{itemName});
    }
    
    public void updateLocationAsPosted(String serverId, String market){
        Log.d(TAG, "Updating location as posted to the server");
        SQLiteDatabase sqldb = getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put("server_id", serverId);
        
        sqldb.update(MarketWatch.Location.TABLE, args, " market = ? ", 
                                                    new String[]{market});
    
    }

    public void updatepriceRequestAsPosted(String serverId, String location, 
            String itemName, String measure){
        Log.d(TAG, "Updating price Request as posted to the server");
        SQLiteDatabase sqldb = getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put("server_id", serverId);

        Cursor cursor = sqldb.query(Location.TABLE, null,
                MarketWatch.Location.MARKET +" = ? ", new String[]{location}, null, null, null);
       
        String locationId = "";
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {              
                locationId = cursor.getString(cursor.getColumnIndex("_id"));                
                cursor.moveToNext();
            }
            cursor.close();
        }
        cursor = null;
        Log.d(TAG, "LocationId "+locationId);
        
        cursor = sqldb.query(Item.TABLE, null,
                MarketWatch.Item.NAME +" = ? ", new String[]{itemName}, null, null, null);
        
        String itemId = "";
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {              
                itemId = cursor.getString(cursor.getColumnIndex("_id"));                
                cursor.moveToNext();
            }
            cursor.close();
        }
        
        cursor = null;
        Log.d(TAG, "ItemId "+itemId);
        
        cursor = sqldb.query(ItemMeasure.TABLE, null,
                MarketWatch.ItemMeasure.NAME +" = ? ", new String[]{measure}, null, null, null);
        
        String itemMeasureId = "";
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {              
                itemMeasureId = cursor.getString(cursor.getColumnIndex("_id"));                
                cursor.moveToNext();
            }
            cursor.close();
        }
        Log.d(TAG, "itemMeasureId "+itemMeasureId);
        sqldb.update(MarketWatch.PriceRequest.TABLE, args, " location_id = ? and item_id = ?"
                + " and measure_id = ?", new String[]{locationId, itemId, itemMeasureId});
    
    }
    
    public void updateItemPriceAsPosted(
        String serverId, String itemName, 
            String location, String measure){
        Log.d(TAG, "Updating price Request as posted to the server");
        SQLiteDatabase sqldb = getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put("server_id", serverId);

        Cursor cursor = sqldb.query(Location.TABLE, null,
                MarketWatch.Location.MARKET +" = ? ", new String[]{location}, null, null, null);
       
        String locationId = "";
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {              
                locationId = cursor.getString(cursor.getColumnIndex("_id"));                
                cursor.moveToNext();
            }
            cursor.close();
        }
        cursor = null;
        Log.d(TAG, "LocationId "+locationId);
        
        cursor = sqldb.query(Item.TABLE, null,
                MarketWatch.Item.NAME +" = ? ", new String[]{itemName}, null, null, null);
        
        String itemId = "";
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {              
                itemId = cursor.getString(cursor.getColumnIndex("_id"));                
                cursor.moveToNext();
            }
            cursor.close();
        }
        
        cursor = null;
        Log.d(TAG, "ItemId "+itemId);
        
        cursor = sqldb.query(ItemMeasure.TABLE, null,
                MarketWatch.ItemMeasure.NAME +" = ? ", new String[]{measure}, null, null, null);
        
        String itemMeasureId = "";
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {              
                itemMeasureId = cursor.getString(cursor.getColumnIndex("_id"));                
                cursor.moveToNext();
            }
            cursor.close();
        }
        Log.d(TAG, "itemMeasureId "+itemMeasureId);
        
        sqldb.update(MarketWatch.ItemPrice.TABLE, args, " location_id = ? and item_id = ?"
                + " and measure_id = ?", new String[]{locationId, itemId, itemMeasureId});
    }
    
    
    public String getServerId(String table, String selection, String selectionArgs[]){
        
        SQLiteDatabase sqldb = getWritableDatabase();
        Cursor cursor = sqldb.query(table, null,
                selection ,selectionArgs, null, null, null);
        
        String serverId = "";
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {              
                serverId = cursor.getString(cursor.getColumnIndex("server_id"));                
                cursor.moveToNext();
            }
            cursor.close();
        }
        Log.d(TAG, "itemMeasureId "+serverId);
        
        return serverId;
    }
}

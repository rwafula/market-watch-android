/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.db;

import android.net.Uri;

/**
 * The MarketWatch provider stores all static information about MarketWatch.
 *
 * @author rube
 */
public class MarketWatch {


    /**
     * The authority of this provider: wfp.com.co.rube
     */
    public static final String AUTHORITY = "wfp.com.co.rube.market_watch";
    /**
     * The content:// style Uri for this provider,
     * content://wfp.com.co.rube.market_watch
     */
    public static final Uri CONTENT_URI = Uri.parse("content://" + MarketWatch.AUTHORITY);
    /**
     * The name of the database file
     */
    static final String DATABASE_NAME = "market_watch";
    /**
     * The version of the database schema
     */
    static final int DATABASE_VERSION = 1;

    /**
     * Columns from the country table.
     *
     * @author rube
     */
    public static class CountryColumns {

        public static final String DIAL_CODE = "dial_code";
        public static final String DIAL_CODE_TYPE = "TEXT";

        public static final String NAME = "name";
        static final String NAME_TYPE = "TEXT";

        public static final String DATE_CREATED = "date_created";
        static final String DATE_CREATED_TYPE = "timestamp NOT NULL default current_timestamp";

        public static final String FLAG_FILE = "flag_file";
        static final String FLAG_FILE_TYPE = "TEXT";

        static final String _ID_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT";
        
        static final String COLUMNS[] = new String[]{
                DIAL_CODE, NAME, DATE_CREATED, FLAG_FILE};

    }

    /**
     * This table contains Countries.
     *
     * @author rube
     */
    public static final class Country extends CountryColumns implements android.provider.BaseColumns {

        /**
         * The MIME type of a CONTENT_URI subdirectory of a single counrty.
         */
        public static final String CONTENT_ITEM_TYPE = "wfp.android.cursor.item/wfp.com.co.rube.country";
        /**
         * The MIME type of CONTENT_URI providing a directory of countries.
         */
        public static final String CONTENT_TYPE = "wfp.android.cursor.dir/wfp.com.co.rube.country";

        /**
         * The content:// style URL for this provider,
         * content://wfp.com.co.rube.market_watch/country
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + MarketWatch.AUTHORITY + "/" + Country.TABLE);

        /**
         * The name of this table
         */
        public static final String TABLE = "country";

        static final String CREATE_STATEMENT
                = " CREATE TABLE IF NOT EXISTS " + Country.TABLE + "(" + " " + Country._ID + " " + Country._ID_TYPE
                + "," + " " + Country.NAME + " " + Country.NAME_TYPE
                + "," + " " + Country.DATE_CREATED + " " + Country.DATE_CREATED_TYPE
                + "," + " " + Country.FLAG_FILE + " " + Country.FLAG_FILE_TYPE
                + "," + " " + Country.DIAL_CODE + " " + Country.DIAL_CODE_TYPE
                + ");";
    }
    
    /**
     * Columns from the items table.
     *
     * @author rube
     */
    public static class ItemMeasureColumns {

        public static final String NAME = "name";
        static final String NAME_TYPE = "TEXT";
        
        public static final String SERVER_ID = "server_id";
        static final String SERVER_ID_TYPE = "INTEGER";

        public static final String DATE_CREATED = "date_created";
        static final String DATE_CREATED_TYPE = "timestamp NOT NULL default current_timestamp";

        static final String _ID_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT";
        
        static final String COLUMNS[] = new String[]{
                NAME, DATE_CREATED };

    }
    
    /**
     * This table contains Items.
     *
     * @author rube
     */
    public static final class ItemMeasure extends ItemMeasureColumns implements android.provider.BaseColumns {

        /**
         * The MIME type of a CONTENT_URI subdirectory of a single counrty.
         */
        public static final String CONTENT_ITEM_TYPE = "wfp.android.cursor.item_measure/wfp.com.co.rube.item_measure";
        /**
         * The MIME type of CONTENT_URI providing a directory of countries.
         */
        public static final String CONTENT_TYPE = "wfp.android.cursor.dir/wfp.com.co.rube.item_measure";

        /**
         * The content:// style URL for this provider,
         * content://wfp.com.co.rube.market_watch/item
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + MarketWatch.AUTHORITY + "/" + ItemMeasure.TABLE);

        /**
         * The name of this table
         */
        public static final String TABLE = "item_measure";

        static final String CREATE_STATEMENT
                = " CREATE TABLE IF NOT EXISTS " + ItemMeasure.TABLE + "(" + " " + ItemMeasure._ID + " " + ItemMeasureColumns._ID_TYPE
                + "," + " " + ItemMeasureColumns.NAME + " " + ItemMeasureColumns.NAME_TYPE
                + "," + " " + ItemMeasureColumns.SERVER_ID + " " + ItemMeasureColumns.SERVER_ID_TYPE
                + "," + " " + ItemMeasureColumns.DATE_CREATED + " " + ItemMeasureColumns.DATE_CREATED_TYPE
                + ");";
    }

    /**
     * Columns from the items table.
     *
     * @author rube
     */
    public static class ItemColumns {

        public static final String NAME = "name";
        static final String NAME_TYPE = "TEXT";
        
        public static final String SERVER_ID = "server_id";
        static final String SERVER_ID_TYPE = "INTEGER";

        public static final String DATE_CREATED = "date_created";
        static final String DATE_CREATED_TYPE = "timestamp NOT NULL default current_timestamp";

        static final String _ID_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT";
        
        static final String COLUMNS[] = new String[]{
                NAME, DATE_CREATED };

    }

    /**
     * This table contains Items.
     *
     * @author rube
     */
    public static final class Item extends ItemColumns implements android.provider.BaseColumns {

        /**
         * The MIME type of a CONTENT_URI subdirectory of a single counrty.
         */
        public static final String CONTENT_ITEM_TYPE = "wfp.android.cursor.item/wfp.com.co.rube.item";
        /**
         * The MIME type of CONTENT_URI providing a directory of countries.
         */
        public static final String CONTENT_TYPE = "wfp.android.cursor.dir/wfp.com.co.rube.item";

        /**
         * The content:// style URL for this provider,
         * content://wfp.com.co.rube.market_watch/item
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + MarketWatch.AUTHORITY + "/" + Item.TABLE);

        /**
         * The name of this table
         */
        public static final String TABLE = "item";

        static final String CREATE_STATEMENT
                = " CREATE TABLE IF NOT EXISTS " + Item.TABLE + "(" + " " + Item._ID + " " + Item._ID_TYPE
                + "," + " " + Item.NAME + " " + Item.NAME_TYPE
                + "," + " " + Item.SERVER_ID + " " + Item.SERVER_ID_TYPE
                + "," + " " + Item.DATE_CREATED + " " + Item.DATE_CREATED_TYPE
                + ");";
    }

    /**
     * Columns from the items price table.
     *
     * @author rube
     */
    public static class ItemPriceColumns {

        public static final String ITEM_ID = "item_id";
        static final String ITEM_ID_TYPE = "INTEGER";
        
        public static final String MEASURE_ID = "measure_id";
        static final String MEASURE_ID_TYPE = "INTEGER";

        public static final String LOCATION_ID = "location_id";
        static final String LOCATION_ID_TYPE = "INTEGER";
        
        public static final String SERVER_ID = "server_id";
        static final String SERVER_ID_TYPE = "INTEGER";

        public static final String PRICE = "price";
        static final String PRICE_TYPE = "REAL NOT NULL";

        public static final String APPROVED = "approved";
        static final String APPROVED_TYPE = "INTEGER ";

        public static final String CREATED_BY = "created_by";
        static final String CREATED_BY_TYPE = "INTEGER ";

        public static final String APPROVED_BY = "approved_by";
        static final String APPROVED_BY_TYPE = "INTEGER ";

        public static final String DATE_CREATED = "date_created";
        static final String DATE_CREATED_TYPE = "timestamp NOT NULL default current_timestamp";

        public static final String DATE_APPROVED = "date_approved";
        static final String DATE_APPROVED_TYPE = "datetime ";

        static final String _ID_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT";
        
        static final String COLUMNS[] = new String[]{
                ITEM_ID, LOCATION_ID, PRICE, APPROVED, CREATED_BY, APPROVED_BY,
                DATE_CREATED, DATE_APPROVED, MEASURE_ID };

    }
    
    /**
     * This table contains Item prices.
     *
     * @author rube
     */
    public static final class ItemPrice extends ItemPriceColumns implements android.provider.BaseColumns {

        /**
         * The MIME type of a CONTENT_URI subdirectory of a single counrty.
         */
        public static final String CONTENT_ITEM_TYPE = "wfp.android.cursor.item/wfp.com.co.rube.item_price";
        /**
         * The MIME type of CONTENT_URI providing a directory of countries.
         */
        public static final String CONTENT_TYPE = "wfp.android.cursor.dir/wfp.com.co.rube.item_price";

        /**
         * The content:// style URL for this provider,
         * content://wfp.com.co.rube.market_watch/item_price
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + MarketWatch.AUTHORITY + "/" + ItemPrice.TABLE);

        /**
         * The name of this table
         */
        public static final String TABLE = "item_price";
        

        static final String CREATE_STATEMENT
                = " CREATE TABLE IF NOT EXISTS " + ItemPrice.TABLE + "(" + " " + ItemPrice._ID + " " + ItemPrice._ID_TYPE
                + "," + " " + ItemPrice.ITEM_ID + " " + ItemPrice.ITEM_ID_TYPE
                + "," + " " + ItemPrice.LOCATION_ID + " " + ItemPrice.LOCATION_ID_TYPE
                + "," + " " + ItemPrice.PRICE + " " + ItemPrice.PRICE_TYPE
                + "," + " " + ItemPrice.MEASURE_ID + " " + ItemPrice.MEASURE_ID_TYPE
                + "," + " " + ItemPrice.SERVER_ID + " " + ItemPrice.SERVER_ID_TYPE
                + "," + " " + ItemPrice.APPROVED + " " + ItemPrice.APPROVED_TYPE
                + "," + " " + ItemPrice.CREATED_BY + " " + ItemPrice.CREATED_BY_TYPE
                + "," + " " + ItemPrice.APPROVED_BY + " " + ItemPrice.APPROVED_BY_TYPE
                + "," + " " + ItemPrice.DATE_CREATED + " " + ItemPrice.DATE_CREATED_TYPE
                + "," + " " + ItemPrice.DATE_APPROVED + " " + ItemPrice.DATE_APPROVED_TYPE                
                + ");";
    }
    
    
    /**
     * Columns from the items price table.
     *
     * @author rube
     */
    public static class LocationColumns {
    
        public static final String COUNTRY_ID = "country_id";
        static final String COUNTRY_ID_TYPE = "INTEGER";

        public static final String STATE = "state";
        static final String STATE_TYPE = "TEXT";
        
        public static final String SERVER_ID = "server_id";
        static final String SERVER_ID_TYPE = "INTEGER";

        public static final String COUNTY = "county";
        static final String COUNTY_TYPE = "TEXT ";

        public static final String MARKET = "market";
        static final String MARKET_TYPE = "TEXT ";

        public static final String DATE_CREATED = "date_created";
        static final String DATE_CREATED_TYPE = "timestamp NOT NULL default current_timestamp";


        static final String _ID_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT";
        
        static final String COLUMNS[] = new String[]{
                COUNTRY_ID, STATE, COUNTY,
                MARKET, SERVER_ID, DATE_CREATED };

    }
    
     
     /**
     * This table contains Item locations.
     *
     * @author rube
     */
    public static final class Location extends LocationColumns implements android.provider.BaseColumns {

        /**
         * The MIME type of a CONTENT_URI subdirectory of a single counrty.
         */
        public static final String CONTENT_ITEM_TYPE = "wfp.android.cursor.item/wfp.com.co.rube.location";
        /**
         * The MIME type of CONTENT_URI providing a directory of countries.
         */
        public static final String CONTENT_TYPE = "wfp.android.cursor.dir/wfp.com.co.rube.location";

        /**
         * The content:// style URL for this provider,
         * content://wfp.com.co.rube.market_watch/location
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + MarketWatch.AUTHORITY + "/" + Location.TABLE);

        /**
         * The name of this table
         */
        public static final String TABLE = "location";
        

        static final String CREATE_STATEMENT
                = " CREATE TABLE IF NOT EXISTS " + Location.TABLE + "(" + " " + Location._ID + " " + Location._ID_TYPE
                + "," + " " + Location.COUNTRY_ID + " " + Location.COUNTRY_ID_TYPE
                + "," + " " + Location.STATE + " " + Location.STATE_TYPE
                + "," + " " + Location.COUNTY + " " + Location.COUNTY_TYPE               
                + "," + " " + Location.MARKET + " " + Location.MARKET_TYPE
                + "," + " " + Location.SERVER_ID + " " + Location.SERVER_ID_TYPE
                + "," + " " + Location.DATE_CREATED + " " + Location.DATE_CREATED_TYPE              
                + ");";
    }

   
/**
     * Columns from the items price price request.
     *
     * @author rube
     */
    public static class PriceRequestColumns {
    
        public static final String USER_ID = "user_id";
        static final String USER_ID_TYPE = "INTEGER";

        public static final String COUNTRY_ID = "country_id";
        static final String COUNTRY_ID_TYPE = "INTEGER";
        
        public static final String LOCATION_ID = "location_id";
        static final String LOCATION_ID_TYPE = "INTEGER";
        
         public static final String MEASURE_ID = "measure_id";
        static final String MEASURE_ID_TYPE = "INTEGER";

        public static final String ORGANIZATION_ID = "organization_id";
        static final String ORGANIZATION_ID_TYPE = "INTEGER ";

        public static final String ITEM_ID = "item_id";
        static final String ITEM_ID_TYPE = "INTEGER ";
        
        public static final String SERVER_ID = "server_id";
        static final String SERVER_ID_TYPE = "INTEGER";
        
        public static final String SERVICED = "serviced";
        static final String SERVICED_TYPE = "INTEGER ";
        
        public static final String DATE_SERVICED = "date_serviced ";
        static final String DATE_SERVICED_TYPE = "datetime";
        
        public static final String DATE_CREATED = "date_created";
        static final String DATE_CREATED_TYPE = "timestamp NOT NULL default current_timestamp";
        
        static final String _ID_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT";
        
        static final String COLUMNS[] = new String[]{
                USER_ID, COUNTRY_ID, ORGANIZATION_ID, ITEM_ID, 
                SERVICED, DATE_SERVICED, DATE_CREATED };       

    }
    
     /**
     * This table contains price requests.
     *
     * @author rube
     */
    public static final class PriceRequest extends PriceRequestColumns implements android.provider.BaseColumns {

        /**
         * The MIME type of a CONTENT_URI subdirectory of a single counrty.
         */
        public static final String CONTENT_ITEM_TYPE = "wfp.android.cursor.item/wfp.com.co.rube.price_request";
        /**
         * The MIME type of CONTENT_URI providing a directory of countries.
         */
        public static final String CONTENT_TYPE = "wfp.android.cursor.dir/wfp.com.co.rube.price_request";

        /**
         * The content:// style URL for this provider,
         * content://wfp.com.co.rube.market_watch/location
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + MarketWatch.AUTHORITY + "/" + PriceRequest.TABLE);

        /**
         * The name of this table
         */
        public static final String TABLE = "price_request";
        

        static final String CREATE_STATEMENT
                = " CREATE TABLE IF NOT EXISTS " + PriceRequest.TABLE + "(" + " " + PriceRequest._ID + " " + PriceRequest._ID_TYPE
                + "," + " " + PriceRequest.COUNTRY_ID + " " + PriceRequest.COUNTRY_ID_TYPE
                + "," + " " + PriceRequest.USER_ID + " " + PriceRequest.USER_ID_TYPE
                + "," + " " + PriceRequest.ORGANIZATION_ID + " " + PriceRequest.ORGANIZATION_ID_TYPE
                + "," + " " + PriceRequest.ITEM_ID + " " + PriceRequest.ITEM_ID_TYPE
                + "," + " " + PriceRequest.LOCATION_ID + " " + PriceRequest.LOCATION_ID_TYPE
                + "," + " " + PriceRequest.MEASURE_ID + " " + PriceRequest.MEASURE_ID_TYPE
                + "," + " " + PriceRequest.SERVICED + " " + PriceRequest.SERVICED_TYPE
                + "," + " " + PriceRequest.SERVER_ID + " " + PriceRequest.SERVER_ID_TYPE
                + "," + " " + PriceRequest.DATE_SERVICED + " " + PriceRequest.DATE_SERVICED_TYPE
                + "," + " " + PriceRequest.DATE_CREATED + " " + PriceRequest.DATE_CREATED_TYPE              
                + ");";
    }
    
    
    public static class PriceResponseColumns {
    
        public static final String REQUEST_ID = "request_id";
        static final String REQUEST_ID_TYPE = "INTEGER";

        public static final String MESSAGE = "message";
        static final String MESSAGE_TYPE = "TEXT";

       
        public static final String DATE_CREATED = "date_created";
        static final String DATE_CREATED_TYPE = "timestamp NOT NULL default current_timestamp";
        
        static final String _ID_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT";
        
        static final String COLUMNS[] = new String[]{
                REQUEST_ID, MESSAGE, DATE_CREATED };

    }
    
    
    /**
     * This table contains price responses.
     *
     * @author rube
     */
    public static final class PriceResponse extends PriceResponseColumns implements android.provider.BaseColumns {

        /**
         * The MIME type of a CONTENT_URI subdirectory of a single counrty.
         */
        public static final String CONTENT_ITEM_TYPE = "wfp.android.cursor.item/wfp.com.co.rube.price_response";
        /**
         * The MIME type of CONTENT_URI providing a directory of countries.
         */
        public static final String CONTENT_TYPE = "wfp.android.cursor.dir/wfp.com.co.rube.price_response";

        /**
         * The content:// style URL for this provider,
         * content://wfp.com.co.rube.market_watch/location
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + MarketWatch.AUTHORITY + "/" + PriceResponse.TABLE);

        /**
         * The name of this table
         */
        public static final String TABLE = "price_response";
        

        static final String CREATE_STATEMENT
                = " CREATE TABLE IF NOT EXISTS " + PriceResponse.TABLE + "(" + " " + PriceResponse._ID + " " + PriceResponse._ID_TYPE
                + "," + " " + PriceResponse.REQUEST_ID + " " + PriceResponse.REQUEST_ID_TYPE
                + "," + " " + PriceResponse.MESSAGE + " " + PriceResponse.MESSAGE_TYPE              
                + "," + " " + PriceResponse.DATE_CREATED + " " + PriceResponse.DATE_CREATED_TYPE              
                + ");";
    }
    
    
    /**
     * Columns from the items table.
     *
     * @author rube
     */
    public static class SyncColumns {

        public static final String NAME = "item";
        static final String NAME_TYPE = "TEXT";

        public static final String DATE_CREATED = "date_created";
        static final String DATE_CREATED_TYPE = "timestamp NOT NULL default current_timestamp";

        static final String _ID_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT";
        
        static final String COLUMNS[] = new String[]{
                NAME, DATE_CREATED };

    }
    
    
    /**
     * This table sync counter.
     *
     * @author rube
     */
    public static final class Sync extends SyncColumns implements android.provider.BaseColumns {

        /**
         * The MIME type of a CONTENT_URI subdirectory of a single counrty.
         */
        public static final String CONTENT_ITEM_TYPE = "wfp.android.cursor.item/wfp.com.co.rube.sync";
        /**
         * The MIME type of CONTENT_URI providing a directory of countries.
         */
        public static final String CONTENT_TYPE = "wfp.android.cursor.dir/wfp.com.co.rube.sync";

        /**
         * The content:// style URL for this provider,
         * content://wfp.com.co.rube.market_watch/location
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + MarketWatch.AUTHORITY + "/" + Sync.TABLE);

        /**
         * The name of this table
         */
        public static final String TABLE = "sync_table";
        

        static final String CREATE_STATEMENT
                = " CREATE TABLE IF NOT EXISTS " + Sync.TABLE + "(" + " " + Sync._ID + " " + Sync._ID_TYPE
                + "," + " " + SyncColumns.NAME + " " + SyncColumns.NAME_TYPE          
                + "," + " " + SyncColumns.DATE_CREATED + " " + SyncColumns.DATE_CREATED_TYPE              
                + ");";
    }
    
}

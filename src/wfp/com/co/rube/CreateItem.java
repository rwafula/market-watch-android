/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import wfp.com.co.rube.db.DbManager;

/**
 *
 * @author rube
 */
public class CreateItem extends Fragment{
    
    private DbManager dbManager;
    private final static String TAG = "wfp.CreateItem";
    public final static String ITEM_NAME = "Items";
    
    /** Called when the activity is first created.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return  */
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, 
            Bundle savedInstanceState) {
      
        View view = inflater.inflate(R.layout.new_item_form, container, false);
        
        final EditText itemNameEditField = (EditText)view.findViewById(R.id.item_name);
        Button submitButton = (Button)view.findViewById(R.id.btn_submit);
        
        Button cancelButton = (Button)view.findViewById(R.id.btn_item_submit_cancel);
        
        if (this.dbManager == null)
        {
           this.dbManager = new DbManager( getActivity().getApplicationContext() );
        }
        
        submitButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) 
            {
 
                String itemName = itemNameEditField.getEditableText().toString();
               
                dbManager.insertItem(itemName);
                Log.d( TAG, "Item submit done: " + itemName);
                Toast.makeText(getActivity(),"Item Created",Toast.LENGTH_SHORT).show();
                loadItemsLists();
                
            }
        });
        
        cancelButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) 
            {
                loadItemsLists();                
            }
        });
 
        return view;
    }
    
    public void loadItemsLists(){
        Fragment fragment = new ItemList();
        Bundle args = new Bundle();   
        args.putString(ItemList.ITEM_NAME,"Item List");
        fragment.setArguments(args);     

        FragmentManager fragmentManager = getFragmentManager();        
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment)
        .commit();
        getActivity().getActionBar().setTitle(ItemList.ITEM_NAME);
    }
    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import wfp.com.co.rube.drawer.CustomDrawerAdapter;
import wfp.com.co.rube.drawer.DrawerItem;

/**
 *
 * @author rube
 */
public class MarketWatch extends Activity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private final static String TAG = "wfp.MarketWatchMain";

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    CustomDrawerAdapter adapter;

    List<DrawerItem> dataList;
    private FragmentManager fragmentManager ;
    private String CUSTOM_FRAGMENT_TAG = "NONE";
    private LinkedList<String> fragmentTagList = new LinkedList<String>();
    
    public static boolean backButtonEnabled  = false;
    
    public Account newAccount;
    
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Create the account type and default account for sync service wouw
        newAccount = new Account("wfp", "wfp.com.co.rube");
        AccountManager accountManager = (AccountManager) this.getSystemService(ACCOUNT_SERVICE);
        // If the account already exists no harm is done but
        // a warning will be logged.
        accountManager.addAccountExplicitly(newAccount, null, null);
        
        ContentResolver.setSyncAutomatically(
                newAccount, wfp.com.co.rube.db.MarketWatch.AUTHORITY, true);
        
        
        setContentView(R.layout.activity_main);

        dataList = new ArrayList<DrawerItem>();
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);

        // Add Drawer Item to dataList
        dataList.add(new DrawerItem("Price List", R.drawable.ic_action_email));
        dataList.add(new DrawerItem("Price Requests", R.drawable.ic_action_email));
        dataList.add(new DrawerItem("Price Response", R.drawable.ic_action_email));
        dataList.add(new DrawerItem("Items", R.drawable.ic_action_good));
        dataList.add(new DrawerItem("Item Measures", R.drawable.ic_action_labels));
        dataList.add(new DrawerItem("Location", R.drawable.ic_action_gamepad));
        //dataList.add(new DrawerItem("Country", R.drawable.ic_action_labels));
        dataList.add(new DrawerItem("About", R.drawable.ic_action_about));
        //dataList.add(new DrawerItem("Settings", R.drawable.ic_action_settings));
        dataList.add(new DrawerItem("Help", R.drawable.ic_action_help));
        Log.i(TAG, "Setting up data list");

        adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item,
                dataList);
        Log.i(TAG, "Setting up data adapter");
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        Log.i(TAG, "Setting up data drawer list");
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.drawer_open,
                R.string.drawer_close) {
                    @Override
                    public void onDrawerClosed(View view) {
                        getActionBar().setTitle(mTitle);
                        invalidateOptionsMenu(); // creates call to
                        // onPrepareOptionsMenu()
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        getActionBar().setTitle(mDrawerTitle);
                        invalidateOptionsMenu(); // creates call to
                        // onPrepareOptionsMenu()
                    }
                };

        Log.i(TAG, "Setting up data mDrawerToggle");

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            SelectItem(0);
        }
        Log.i(TAG, "Setting up data fragment next");
        Fragment marketPrice = new MarketPrice();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, marketPrice);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    public void SelectItem(int position) {

        Fragment fragment = null;
        Bundle args = new Bundle();

        fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();
        Log.i(TAG, "Changing fragment to " + position);        
        switch (position) {
            case 0:
                // price list
                fragment = new MarketPrice();
                args.putString(MarketPrice.ITEM_NAME, dataList.get(position)
                        .getItemName());
                fragment.setArguments(args); 
                CUSTOM_FRAGMENT_TAG = "Market Prices";
                break;

            case 1:
                // Price request
                fragment = new PriceRequestList();
                args.putString(PriceRequestList.ITEM_NAME, dataList.get(position)
                        .getItemName());
                fragment.setArguments(args);  
                CUSTOM_FRAGMENT_TAG = "Price Request List";
                break;

            case 2:
                // Price response
                fragment = new PriceResponseList();   
                CUSTOM_FRAGMENT_TAG = "Price Response List";
                break;

            case 3:
                // items
                fragment = new ItemList();
                args.putString(MarketPrice.ITEM_NAME, dataList.get(position)
                        .getItemName());
                fragment.setArguments(args);
                CUSTOM_FRAGMENT_TAG = "Item List";
                break;
            case 4:
                // items
                fragment = new ItemMeasureList();
                args.putString(ItemMeasureList.ITEM_NAME, dataList.get(position)
                        .getItemName());
                fragment.setArguments(args);
                CUSTOM_FRAGMENT_TAG = "Item Measure List";
                break;

            case 5:
                // Location
                fragment = new LocationList();
                args.putString(LocationList.ITEM_NAME, dataList.get(position)
                        .getItemName());
                fragment.setArguments(args);
                CUSTOM_FRAGMENT_TAG = "Location List";
                
                
                 break;
            case 6:
                // Country
                fragment = new CountryList();  
                CUSTOM_FRAGMENT_TAG = "Country List";
                break;
                
            case 7:
                // About
                fragment = new AboutTextFragment();    
                CUSTOM_FRAGMENT_TAG = "About";
                break;
                
            case 8:
                //Help
                fragment = new HelpTextFragment(); 
                CUSTOM_FRAGMENT_TAG = "Help";
                break;

            default:
                fragment = new MarketPrice();
                args.putString(MarketPrice.ITEM_NAME, dataList.get(position)
                        .getItemName());
                fragment.setArguments(args);    
                CUSTOM_FRAGMENT_TAG = "Market Prices";
        }
 
        fragmentTransaction.replace(R.id.content_frame, fragment, CUSTOM_FRAGMENT_TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        fragmentTagList.add(CUSTOM_FRAGMENT_TAG);
        mDrawerList.setItemChecked(position, true);
        setTitle(dataList.get(position).getItemName());
        mDrawerLayout.closeDrawer(mDrawerList);
        MarketWatch.backButtonEnabled = true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        
        int selectedId = item.getItemId();
        if(selectedId == R.id.action_sync){
            this.onRefreshButtonClick();
            return true;
        } else {
        
            return mDrawerToggle.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            SelectItem(position);

        }
    }
    
    @Override
    public void onBackPressed() {
        //No back when you hit price list tab
        CUSTOM_FRAGMENT_TAG = fragmentTagList.pollLast();
        
        Log.d(TAG, "Hitting back on "+CUSTOM_FRAGMENT_TAG);  
        
        
        if (CUSTOM_FRAGMENT_TAG != null){
            
            if (!CUSTOM_FRAGMENT_TAG.equals("MARKETPRICE") && MarketWatch.backButtonEnabled) { // and then you define a method allowBackPressed with the logic to allow back pressed or not
                super.onBackPressed();
            }
            
            String title = "Market Watch";
            try {
                title= fragmentTagList.getLast();
            } catch (NoSuchElementException e) {
                //Hit empty list ignore
                
            }
            setTitle(title);
            
        }
        
        //Clear if backButton is disabbled
         if (!MarketWatch.backButtonEnabled && !fragmentTagList.isEmpty()){
             fragmentTagList.clear();
         }
        
    }
    
    /**
     * Respond to a button click by calling requestSync(). This is an
     * asynchronous operation.
     */
    
    public void onRefreshButtonClick() {
        Log.d(TAG, "Found actions on click manual sync");
        // Pass the settings flags by inserting them in a bundle
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        /*
         * Request the sync for the default account, authority, and
         * manual sync settings
         */
        AccountManager accountManager = (AccountManager) this.getSystemService(ACCOUNT_SERVICE);
        // If the account already exists no harm is done but
        // a warning will be logged.
        if(newAccount == null){
            newAccount =  new Account("wfp", "wfp.com.co.rube");
        }
        accountManager.addAccountExplicitly(newAccount, null, null);

        ContentResolver.requestSync(newAccount, 
                wfp.com.co.rube.db.MarketWatch.AUTHORITY, settingsBundle);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.entities;


/**
 *
 * @author rube
 */
public class Location {
    private String stateName;
    private String payam;
    private String county;
    private String market;
    private String dateCreated;
    private String serverId;
   
    /**
     * @return the countryName
     */
    public String getStateName() {
        return stateName;
    }

    /**
     * @param state
     */
    public void setStateName(String state) {
        this.stateName = state;
    }

    /**
     * @return the city
     */
    public String getPayam() {
        return  payam ;
    }

    /**
     * @param payam
     */
    public void setPayam(String payam) {
        this.payam = payam;
    }

    /**
     * @return the county
     */
    public String getCounty() {
        return county;
    }

    /**
     * @param county
     */
    public void setCounty(String county) {
        this.county = county;
    }




    /**
     * @return the market
     */
    public String getMarket() {
        return market;
    }

    /**
     * @param market the market to set
     */
    public void setMarket(String market) {
        this.market = market;
    }

    /**
     * @return the dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

   
    
    @Override
    public String toString(){
        return stateName + " - "+payam + " - " + market;
    } 

    /**
     * @return the serverId
     */
    public String getServerId() {
        return serverId;
    }

    /**
     * @param serverId the serverId to set
     */
    public void setServerId(String serverId) {
        this.serverId = serverId;
    }
}



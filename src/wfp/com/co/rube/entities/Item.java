/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.entities;

/**
 *
 * @author rube
 */

public class Item {
    private String itemName;
    private String createDate;
    
    public String getItemName() {
            return itemName;
    }
    
    public void setItemName(String itemName) {
            this.itemName = itemName;
    }

    public String getCreateDate() {
            return this.createDate;
    }
    
    public void setCreateDate(String createDate) {
         this.createDate = createDate;
    }

    @Override
    public String toString() {
            return "[ item=" + itemName + ", Create Date =" + createDate + " ]";
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.entities;

/**
 *
 * @author rube
 */
public class PriceResponse {
    private PriceRequest request;
    private String dateCreated;
    private String message;

    /**
     * @return the request
     */
    public PriceRequest getRequest() {
        return request;
    }

    /**
     * @param request the request to set
     */
    public void setRequest(PriceRequest request) {
        this.request = request;
    }

    /**
     * @return the dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    @Override
    public String toString(){
        return "Location: "+request+ "Message: "+message;
    
    }
    
    
}

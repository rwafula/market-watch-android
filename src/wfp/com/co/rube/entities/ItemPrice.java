/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.entities;

/**
 *
 * @author rube
 */
public class ItemPrice {
    private String itemName;
    private String price;
    private String locationName;
    private String date;
    private String measure;
    private String isSynced;
    
    public String getIsSynced(){
        return isSynced;
    }

    public void setIsSynced(String value){
        this.isSynced = value;
    }
    
    public String getItemName() {
            return itemName;
    }
    
    

    public void setItemName(String itemName) {
            this.itemName = itemName;
    }

    public String getPrice() {
            return price;
    }
    
    public String getLocation() {
            return locationName;
    }
     
    public void setLocation(String locationName) {
            this.locationName = locationName;
    }

    public void setPrice(String price) {
         this.price = price;
    }

    public String getDate() {
            return date;
    }

    public void setDate(String date) {
            this.date = date;
    }



    /**
     * @return the measure
     */
    public String getMeasure() {
        return measure;
    }

    /**
     * @param measure the measure to set
     */
    public void setMeasure(String measure) {
        this.measure = measure;
    }
    
    @Override
    public String toString() {
            return "[ item=" + itemName + ", location=" + locationName + " "
                    + ", price="+price + " date=" + date + "]";
    }
}
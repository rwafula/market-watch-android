/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.entities;

/**
 *
 * @author rube
 */
public class Country {
    

    private String name;
    private String code;
    private String dialCode;
    private String createDate;
    
    public String getName() {
            return name;
    }
    
    public void setName(String name) {
            this.name = name;
    }
    
    public String getDialCode() {
            return dialCode;
    }
    
    public void setDialCode(String dialCode) {
            this.dialCode = dialCode;
    }

    public String getCreateDate() {
            return this.createDate;
    }
    
    public void setCreateDate(String createDate) {
         this.createDate = createDate;
    }

    @Override
    public String toString() {
            return "[ Country=" + name + ", Code =" + code + " ]";
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }
}

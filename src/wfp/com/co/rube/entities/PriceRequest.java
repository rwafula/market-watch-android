/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.entities;

/**
 *
 * @author rube
 */
public class PriceRequest {
    
    private String itemName;
    private String measure;
    private String location;
    private String createDate;
    
    public String getItemName() {
            return itemName;
    }
    
    public void setItemName(String itemName) {
            this.itemName = itemName;
    }

    public String getCreateDate() {
            return this.createDate;
    }
    
    public void setCreateDate(String createDate) {
         this.createDate = createDate;
    }

    /**
     * @return the measure
     */
    public String getMeasure() {
        return this.measure;
    }

    /**
     * @param measure the measure to set
     */
    public void setMeasure(String measure) {
        this.measure = measure;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return  location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }
    
    @Override
    public String toString() {
            return "[ item=" + itemName + ", Measure = "+measure+", "
                    + " Location = "+location+", Create Date =" + createDate + " ]";
    }
}
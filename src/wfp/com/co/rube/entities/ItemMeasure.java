/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wfp.com.co.rube.entities;

/**
 *
 * @author rube
 */

public class ItemMeasure {
    private String name;
    private String serverId;
    private String createDate;
    
    public String getItemName() {
            return name;
    }
    
    public void setItemName(String itemName) {
            this.name = itemName;
    }

    public String getCreateDate() {
            return this.createDate;
    }
    
    public void setCreateDate(String createDate) {
         this.createDate = createDate;
    }

    @Override
    public String toString() {
            return "[ item=" + name + ", Create Date =" + createDate + " ]";
    }

    /**
     * @return the serverId
     */
    public String getServerId() {        
        return serverId;
    }

    /**
     * @param serverId the serverId to set
     */
    public void setServerId(String serverId) {
        this.serverId = serverId;
    }
}
